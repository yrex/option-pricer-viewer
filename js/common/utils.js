

const isUndefined = (v) => {
    return typeof(v) === 'undefined' || v === null;
};


const isEmpty = (obj) => {
    for (var key in obj) {
        if (obj.hasOwnProperty(key)) {
            return false;
        }
    }
    return true;
};


const countItems = (arr) => {
    var count = 0;
    arr.forEach((v) => {
        if (typeof(v) !== 'undefined') {
            count += 1;
        }
    });

    return count;
};


/**
 * Custom error class used for rejecting a promise due when requesting
 * data for an invalid scenario.
 * @param {string} message - The human-readable error message
 */
function InvalidScenarioError(message) {
    this.message = message;
}
InvalidScenarioError.prototype = new Error();


/**
 * Check if two arrays are the same. The order of elements in the array is considered.
 * Comparison can be performed on nested arrays.
 * Limitation: The elements cannot be objects.
 * @param {Array} array1 - the first array
 * @param {Array} array2 - the second array
 * @returns {boolean}
 */
function isArrayEqual(array1, array2) {
    return (array1.length === array2.length) && array1.every(
            (val, idx) => {
                if (val instanceof Array && array2[idx] instanceof Array) {
                    return isArrayEqual(val, array2[idx]);
                }
                return val === array2[idx];
            });
}

/**
 * Check if the input is a number
 * From: < http://stackoverflow.com/questions/9716468/ >
 * @param input
 * @returns {boolean} - whether the input is a number or not.
 */
function isNumeric(input) {
    return !isNaN(parseFloat(input)) && isFinite(input);
}

/**
 * Round the input number to the given number of decimal points.
 * If the number is greater than 1e6, it will instead be rounded
 * to the given number of significant digits.
 * @param {Number} number
 * @param {Number} numDecPoints - This should be an integer
 * @returns {Number} the rounded number
 */
function formatNumber(number, numDecPoints) {
    if (isNumeric(number)) {
        if (number > 1e6) {
            return parseFloat(number.toPrecision(numDecPoints));
        }
        else {
            return parseFloat(number.toFixed(numDecPoints));
        }
    }
    return number;
}


module.exports = {
    isUndefined,
    isEmpty,
    countItems,
    InvalidScenarioError,
    isArrayEqual,
    isNumeric,
    formatNumber,
};
