import keyMirror from 'keymirror';


const LegConstants = keyMirror({
    ADD_LEG: null,
    UPDATE_LEG: null,
    REMOVE_LEG: null,
    INIT: null,
    SET_LEG_PRESET: null,
});

const DEFAULT_LEG_TYPE = 'long_call';
const STRATEGY_JSON_FILE = 'static/data/strategies.json';


module.exports = {
    LegConstants,
    DEFAULT_LEG_TYPE,
    STRATEGY_JSON_FILE,
};
