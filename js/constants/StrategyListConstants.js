import keyMirror from 'keymirror';


const StrategyListConstants = keyMirror({
    TOGGLE_DISPLAY: null,
});


export default StrategyListConstants;
