import keyMirror from 'keymirror';
import { BSParams, ReturnTypes } from '../constants/APIConstants';


const ScenarioConfigConstants = keyMirror({
    ADD_SCENARIO: null,
    UPDATE_SCENARIO: null,
    REMOVE_SCENARIO: null,
    SET_SCENARIO_VALIDATION: null,
});

const Scenario_xVals = [
    { name: 'Price', value: BSParams.S },
    { name: 'Strike', value: BSParams.K },
    { name: 'Dividend', value: BSParams.D },
    { name: 'Time to Expiry', value: BSParams.T },
    { name: 'Vol', value: BSParams.SIGMA },
    { name: 'Rate', value: BSParams.R },
];

const Scenario_yVals = [
    { name: 'Value', value: ReturnTypes.VALUE },
    { name: 'Delta', value: ReturnTypes.DELTA },
    { name: 'Gamma', value: ReturnTypes.GAMMA },
    { name: 'Theta', value: ReturnTypes.THETA },
    { name: 'Vega', value: ReturnTypes.VEGA },
    { name: 'Rho', value: ReturnTypes.RHO },
];

module.exports = {
    ScenarioConfigConstants,
    Scenario_xVals,
    Scenario_yVals,
};

