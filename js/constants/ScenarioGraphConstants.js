import keyMirror from 'keymirror';


const ScenarioGraphConstants = keyMirror({
    UPDATE_GRAPH: null,
    CLEAR_GRAPH_ADD_MESSAGE: null,
    TOGGLE_GRAPH_LEGBYLEG: null,
});


export default ScenarioGraphConstants;
