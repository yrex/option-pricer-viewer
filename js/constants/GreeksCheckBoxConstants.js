import keyMirror from 'keymirror';


const GreeksCheckBoxConstants = keyMirror({
    TOGGLE_CHECKED: null,
});


export default GreeksCheckBoxConstants;
