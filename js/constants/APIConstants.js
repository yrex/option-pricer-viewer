import keyMirror from 'keymirror';

// set the PROD_API_URL and DEV_API_URL parameters in package.json.
// You can override the value by setting it in your ~/.npmrc file.
const API_ROOT =
    process.env.NODE_ENV !== 'production' ?
        process.env.npm_package_config_DEV_API_URL : process.env.npm_package_config_PROD_API_URL;

const ApiDetails = {
    root_url: API_ROOT,
    multileg_pricer_url: API_ROOT + '/api/pricer/multi_leg/black_scholes',
    scenario_pricer_url: API_ROOT + '/api/pricer/scenario/black_scholes',
};

const CallPut = {
    call: 'C',
    put: 'P',
};

const BuySell = {
    buy: 'B',
    sell: 'S',
};

const Greeks = {
    value: 'value',
    delta: 'delta',
    gamma: 'gamma',
    theta: 'theta',
    vega: 'vega',
    rho: 'rho',
};

const BSParams = keyMirror({
    S: null,
    K: null,
    D: null,
    T: null,
    SIGMA: null,
    R: null,
});

const ReturnTypes = keyMirror({
    VALUE: null,
    DELTA: null,
    GAMMA: null,
    THETA: null,
    VEGA: null,
    RHO: null,
});


module.exports = {
    ApiDetails,
    CallPut,
    BuySell,
    Greeks,
    BSParams,
    ReturnTypes,
};
