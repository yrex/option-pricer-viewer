import keyMirror from 'keymirror';


const MessageTypes = keyMirror({
    ERROR: null,
    WARNING: null,
    INFO: null,
});

const MessageEvents = keyMirror({
    DELETE_MESSAGE: null,
    ADD_MESSAGE: null,
    ADD_MESSAGE_CLEAR_RESULTS: null,
    DELETE__MESSAGES_BY_TYPE: null,
});

const MessageLabels = keyMirror({
    PRICER: null,
    SCENARIO: null,
});


module.exports = {
    MessageTypes,
    MessageEvents,
    MessageLabels,
};
