
const CHANGE_EVENT = 'change';
const INIT_EVENT = 'init';
const UPDATE_EVENT = 'update';

module.exports = {
    CHANGE_EVENT,
    INIT_EVENT,
    UPDATE_EVENT,
};
