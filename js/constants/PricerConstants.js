import keyMirror from 'keymirror';


const PricerConstants = keyMirror({
    SET_VALID: null,
});


export default PricerConstants;