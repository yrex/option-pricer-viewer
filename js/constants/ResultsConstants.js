import keyMirror from 'keymirror';


const ResultsConstants = keyMirror({
    UPDATE_RESULTS: null,
    TOGGLE_LEGBYLEG: null,
    CLEAR_RESULTS: null,
});

const NumDecimalPlaces = 3;

module.exports = {
    ResultsConstants,
    NumDecimalPlaces,
};
