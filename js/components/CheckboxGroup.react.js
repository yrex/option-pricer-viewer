import React from 'react';
import GreeksCheckBoxStoreActions from '../actions/GreeksCheckBoxStoreActions';
import CheckBoxWrapper from './CheckboxWrapper.react';
import Joi from 'joi-browser';
import validation from 'react-validation-mixin';
import ValidationStrategy from 'joi-browser-validation-strategy';
import classnames from 'classnames';


class CheckboxGroup extends React.Component {
    constructor(props) {
        super(props);

        this.validatorTypes = {
            checkboxGroup: Joi.array().items(
                Joi.boolean().valid(true).required().label('selection'),
                Joi.boolean().required())
                .label('greeks'),
        };

        this._onChange = this._onChange.bind(this);
        this.getValidatorData = this.getValidatorData.bind(this);
        this.renderHelpText = this.renderHelpText.bind(this);
        this.getClasses = this.getClasses.bind(this);
    }

    _onChange(idx, callback) {
        return () => {
            GreeksCheckBoxStoreActions.toggleChecked(idx);
            if (typeof callback === 'function') {
                callback();
            }
        };
    }

    getValidatorData() {
        return { checkboxGroup: this.props.data.map(d => d.checked) };
    }

    renderHelpText(message) {
        return (
            <span className="help-block">{message}</span>
        );
    }

    getClasses(field) {
        return classnames({
            'checkbox-group': true,
            'has-error': !this.props.isValid(field),
        });
    }

    handleValidation() {
        if (process.env.NODE_ENV !== 'production') {
            console.log(this.props.data);
        }
        this.props.handleValidation('checkboxGroup',
            this.props.logValidationState('checkboxgroup', this.isAtLeastOneChecked()))();
    }

    isAtLeastOneChecked() {
        const val = this.props.data.reduce(
            (prev, curr) => ({ checked: prev.checked || curr.checked })
        );
        return val.checked;
    }


    render() {
        const checkboxes = this.props.data.map( (d, idx) => {
            return (
                <CheckBoxWrapper
                    {...d}
                    key={ idx }
                    onChangeHandler={ this._onChange(idx, this.handleValidation.bind(this)) }
                />
            );
        });

        return (
            <div>
                <fieldset className={this.getClasses('checkboxGroup')}>
                    { checkboxes }
                </fieldset>
                { this.renderHelpText(this.props.getValidationMessages('checkboxGroup')) }
            </div>
        );
    }
}

CheckboxGroup.PropTypes = {
    data: React.PropTypes.object.isRequired,
    logValidationState: React.PropTypes.func.isRequired,
    errors: React.PropTypes.object,
    validate: React.PropTypes.func,
    isValid: React.PropTypes.func,
    handleValidation: React.PropTypes.func,
    getValidationMessages: React.PropTypes.func,
    clearValidations: React.PropTypes.func,
};


export default validation(ValidationStrategy)(CheckboxGroup);
