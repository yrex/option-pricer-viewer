import React from 'react';
import LegStoreActions from '../actions/LegActions';
import LegResultCombo from './LegResultCombo.react';
import Slider from 'react-slick';
import debounce from 'lodash.debounce';
import StrategyListStoreActions from '../actions/StrategyListStoreActions';
import classnames from 'classnames';


class DealResultCombo extends React.Component {

    constructor(props) {
        super(props);

        this.state = { numSlides: this._calcNumSlides() };

        this.AddLeg = this.AddLeg.bind(this);
        this.showStrategyMenu = this.showStrategyMenu.bind(this);
        this.getDealClasses = this.getDealClasses.bind(this);
        this._calcNumSlides = this._calcNumSlides.bind(this);
        this._handleWindowResize = debounce(this._handleWindowResize.bind(this), 100);
    }

    componentDidMount() {
        window.addEventListener('resize', this._handleWindowResize);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this._handleWindowResize);
    }

    _calcNumSlides() {
        var slideFraction = 0.1;
        const slideWidth = Math.floor(this.props.maxWidth / this.props.numSlides);

        // default slide size
        var numSlides = this.props.numSlides + slideFraction;

        var i;
        for (i = 1; i <= this.props.numSlides; i++) {
            if (window.innerWidth > (slideWidth * (i - 1)) &&
                window.innerWidth < (slideWidth * i)) {
                numSlides = i + slideFraction;
                break;
            }
        }

        return numSlides;
    }

    _handleWindowResize() {
        var numSlides = this._calcNumSlides();

        this.setState({ numSlides: numSlides });

        // console.log(this.state);
        // console.log(window.innerWidth);
    }

    /**
     * Add a leg to the deal. Call an Action to append an an item to the LegStore.
     * @param e {object} event object.
     */
    AddLeg(e) {
        e.preventDefault();
        LegStoreActions.add();
    }

    showStrategyMenu() {
        StrategyListStoreActions.toggleDisplay();
    }

    getDealClasses(mainClass, overflowFlag) {
        return classnames(mainClass, {
            'overflow': overflowFlag,
        });
    }

    render() {
        const legs = this.props.legs;
        const legResults = this.props.legResults;
        var carouselOverflow = false;


        const legHtml = [];
        var legNum = 0;
        var legResult;
        const logValidationState = this.props.logValidationState;
        legs.forEach((leg, idx) => {
            legNum += 1;
            legResult = legResults[idx];
            legHtml.push(
                <div key={ idx }>
                    <LegResultCombo
                        key={ idx }
                        id={ idx }
                        legNum={ legNum }
                        params={ leg }
                        name={'leg' + idx }
                        logValidationState={ logValidationState }
                        legResult = { legResult }
                    />
                </div>
            );
        });

        if (legNum > this.state.numSlides) {
            carouselOverflow = true;
        }

        var settings = {
            dots: true,
            slidesToShow: this.state.numSlides,
            slidesToScroll: 1,
            swipeToSlide: false,
            centerMode: false,
            swipe: true,
            draggable: true,
            initialSlide: 0,
            infinite: false,
            beforeChange: function beforeChange(currentSlide, nextSlide) {
                if (process.env.NODE_ENV !== 'production') {
                    console.log('before change', currentSlide, nextSlide);
                }
            },
            afterChange: function afterChange(currentSlide) {
                if (process.env.NODE_ENV !== 'production') {
                    console.log('after change', currentSlide);
                }
            },
        };

        return (
            <div className={ this.getDealClasses('deal', carouselOverflow) }>
                <div className="deal-header">
                    <h3>Strategy: <span className="strategy-name">{ this.props.name }</span></h3>

                    <button type="button" className="add-button" onClick={ this.AddLeg }>
                        <span className="fa fa-fw fa-plus-circle"></span>
                        <span className="button-text">Add Contract</span>
                    </button>

                    <button type="button" className="strategy-button" onClick={ this.showStrategyMenu }>
                        <span className="fa fa-fw fa-sliders"></span>
                        <span className="button-text">Choose Other Strategy</span>
                    </button>
                </div>

                <Slider {...settings}>
                    { legHtml }
                </Slider>
            </div>
        );
    }
}

DealResultCombo.propTypes = {
    legs: React.PropTypes.array.isRequired,
    numLegs: React.PropTypes.number.isRequired,
    options: React.PropTypes.array.isRequired,
    name: React.PropTypes.string.isRequired,
    logValidationState: React.PropTypes.func.isRequired,
    legResults: React.PropTypes.array.isRequired,
    maxWidth: React.PropTypes.number.isRequired,
    numSlides: React.PropTypes.number.isRequired,
};


export default DealResultCombo;
