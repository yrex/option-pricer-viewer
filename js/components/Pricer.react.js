import React from 'react';
import LegStore from '../stores/LegStore';
import MessageStoreActions from '../actions/MessageStoreActions';
import GreeksCheckBoxStore from '../stores/GreeksCheckBoxStore.js';
import CheckBoxGroup from './CheckboxGroup.react';
import { ResultsActionCreator } from '../actions/ResultsStoreActions';
import { MessageTypes } from '../constants/MessageConstants';
import { MessageLabels } from '../constants/MessageConstants';
import assign from 'object-assign';
import PricerStoreActions from '../actions/PricerStoreActions';
import PricerStore from '../stores/PricerStore';
import { ResultsStore } from '../stores/ResultsStore';
import DealResultCombo from './DealResultCombo.react';
import classnames from 'classnames';
import { MAX_VIEW_WIDTH, NUM_CAROUSEL_SLIDES } from '../constants/StyleConstants';


function getItemState() {
    return {
        legs: LegStore.getAllLegs(),
        numLegs: LegStore.getNumLegs(),
        dealName: LegStore.getCurrentStrategyName(),
        greeksCheckBoxData: GreeksCheckBoxStore.getData(),
        isValid: PricerStore.getValidationState(),
    };
}


class Pricer extends React.Component {

    constructor(props) {
        super(props);

        var stateParams = getItemState();
        stateParams.childValidationState = {};

        this.state = stateParams;

        this._onSubmit = this._onSubmit.bind(this);
        this._onKeyPress = this._onKeyPress.bind(this);
        this._onChange = this._onChange.bind(this);
        this.isPricerValid2 = this.isPricerValid2.bind(this);
        this.logValidationState = this.logValidationState.bind(this);
    }

    componentDidMount() {
        LegStore.addChangeListener(this._onChange);
        LegStore.addInitListener(this._onChange);
        GreeksCheckBoxStore.addChangeListener(this._onChange);
        PricerStore.addChangeListener(this._onChange);
        ResultsStore.addChangeListener(this._onChange);
        ResultsStore.addInitListener(this._onChange);
    }

    componentWillUnmount() {
        LegStore.removeChangeListener(this._onChange);
        LegStore.removeInitListener(this._onChange);
        GreeksCheckBoxStore.removeChangeListener(this._onChange);
        PricerStore.addChangeListener(this._onChange);
        ResultsStore.removeChangeListener(this._onChange);
        ResultsStore.removeInitListener(this._onChange);
    }

    _onChange() {
        if (process.env.NODE_ENV !== 'production') {
            console.log('%%%%% BING %%%%%%%');
            console.trace();
        }
        this.setState(getItemState());
    }

    _onSubmit(e) {
        e.preventDefault();

        if (this.state.numLegs === 0) {
            var msg = "No deal to price!";
            MessageStoreActions.AddMessageAndClearResults(msg, MessageTypes.WARNING, MessageLabels.PRICER);
        }
        else {
            //ResultsActionCreator.APIGet();
            ResultsActionCreator.APIGetWithScenarios();
        }
    }

    _onKeyPress(e) {
        // disable form submit on enter key press
        if (e.key === 'Enter') {
            if (process.env.NODE_ENV !== 'production') {
                console.log('No enter key!');
            }
            e.preventDefault();
        }
    }

    /**
     * Keep a log of the validation state of each component that registers
     * its validation after an event
     * @param name {string} unique name for component
     * @param value {boolean} whether the component is valid or not
     */
    logValidationState(name, value) {
        if (process.env.NODE_ENV !== 'production') {
            console.log(name + ': ' + value);
        }

        // register component validation
        const newChildValidationState = assign({}, this.state.childValidationState);
        newChildValidationState[name] = value;

        // check new validation state for pricer
        const isValid = this.isPricerValid2(newChildValidationState);
        const prev_state = this.state.isValid;

        this.setState({
            childValidationState: newChildValidationState,
        });

        // send a validation status change update, if the validation state has changed
        if (isValid !== prev_state) {
            PricerStoreActions.setValidation(isValid);
        }

    }

    /**
     * Check if all components are valid.
     * Components would have to register their valid state to the childValidationState.
     * @return {boolean} returns true if form is valid.
     */
    isPricerValid2(registeredStates) {
        var isValid = this.state.numLegs !== 0;
        for (var key in registeredStates) {
            if (registeredStates.hasOwnProperty(key)) {
                if (!registeredStates[key]) {
                    isValid = false;
                    break;
                }
            }
        }
        return isValid;
    }

    /**
     * Create the class names used for the submit button.
     * One class is controlled by a boolean flag, which is enabled if a recalculation is required.
     * @param {boolean} flag - When set to True, the class name for recalculation is enabled.
     * @returns {*} A string containing the class names is returned.
     */
    getSubmitClasses(flag) {
        return classnames({
            'fa': true,
            'fa-fw': true,
            'fa-arrow-circle-right': !flag,
            'fa-bolt': flag,
        });
    }


    render() {
        const canSubmit = this.state.isValid;
        const legResults = ResultsStore.getResultsLegByLegFormatted();
        const shouldUpdate = ResultsStore.needResultRefresh();
        var refreshMsg = '';
        if (shouldUpdate && canSubmit) {
            refreshMsg = 'Run the calculations to update results';
        }

        return (
            <form
                onSubmit={this._onSubmit}
                onKeyPress={this._onKeyPress}
            >

                <DealResultCombo
                    numLegs={ this.state.numLegs }
                    legs={ this.state.legs }
                    options={ this.state.greeksCheckBoxData }
                    name={ this.state.dealName }
                    logValidationState={ this.logValidationState }
                    legResults = { legResults }
                    maxWidth={ MAX_VIEW_WIDTH }
                    numSlides={ NUM_CAROUSEL_SLIDES }
                />

                <CheckBoxGroup
                    data={ this.state.greeksCheckBoxData }
                    logValidationState={ this.logValidationState }
                />

                <div className="calc-box">
                    <button type="submit" className="calc-button" disabled= { !canSubmit }>
                        <span className={ this.getSubmitClasses(shouldUpdate && canSubmit) }></span>
                        <span className="button-text">Run calculations</span>
                    </button>

                    <span className="message">{ refreshMsg }</span>
                </div>

            </form>
        );
    }
}


export default Pricer;
