import React from 'react';
import LegStoreActions from '../actions/LegActions';
import Leg from './Leg.react';


class Deal extends React.Component {

    constructor(props) {
        super(props);
        this.AddLeg = this.AddLeg.bind(this);
    }

    /**
     * Add a leg to the deal. Call an Action to append an an item to the LegStore.
     * @param e {object} event object.
     */
    AddLeg(e) {
        e.preventDefault();
        LegStoreActions.add();
    }

    render() {
        const legs = this.props.legs;


        const legHtml = [];
        var legNum = 0;
        const logValidationState = this.props.logValidationState;
        legs.forEach((leg, idx) => {
            legNum += 1;
            legHtml.push(
                <Leg
                    key={ idx }
                    id={ idx }
                    legNum={ legNum }
                    params={ leg }
                    name={'leg' + idx }
                    logValidationState={ logValidationState }
                />
            );
        });

        return (

            <div className="deal">
                <h3>{ this.props.name }</h3>
                { legHtml }
                <button type="button" className="add-button" onClick={ this.AddLeg }>
                    <span className="fa fa-fw fa-plus-circle"></span><span className="button-text">Add Contract</span>
                </button>
            </div>
        );
    }
}

Deal.propTypes = {
    legs: React.PropTypes.array.isRequired,
    numLegs: React.PropTypes.number.isRequired,
    options: React.PropTypes.array.isRequired,
    name: React.PropTypes.string.isRequired,
    logValidationState: React.PropTypes.func.isRequired,
};


export default Deal;
