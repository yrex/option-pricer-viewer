import React from 'react';
import { ResultsStore } from '../stores/ResultsStore.js';
import { ResultsStoreActions } from '../actions/ResultsStoreActions';
import { NumDecimalPlaces } from '../constants/ResultsConstants.js';
import FormattedNumber from './FormattedNumber.react';
import classnames from 'classnames';
import MessageList from './MessageList.react.js';
import MessageStore from '../stores/MessageStore';
import { MessageLabels } from '../constants/MessageConstants';
import { isUndefined } from '../common/utils';


class Results extends React.Component{
    constructor(props) {
        super(props);
        this._onChange = this._onChange.bind(this);
        this.resultsByLeg = this.resultsByLeg.bind(this);
        this.toggleConfigClass = this.toggleConfigClass.bind(this);
        this.getMessagesForLabel = this.getMessagesForLabel.bind(this);
        this._getValue = this._getValue.bind(this);
    }


    _onChange() {
        ResultsStoreActions.toggleLegByLeg();
    }

    toggleConfigClass(name) {
        return classnames(name, {
            // 'scenario-subheader': true,
            'by-contract-enabled': this.props.LegByLegChecked,
            'by-contract-disabled': !this.props.LegByLegChecked,
        });
    }


    getMessagesForLabel(label) {
        return MessageStore.getMessagesForLabel(label);
    }


    _getValue(inputArray, index) {
        var output = 0;
        if (!isUndefined(inputArray)) {
            output = inputArray[index];
        }
        return output;
    }


    resultsByLeg() {
        var val = '';
        var legNum = 0;

        const legResultsHtml = [];
        if (this.props.LegByLegChecked) {
            // var templateArray;
            // if (!isUndefined(this.props.results_legbyleg.value)) {
            //     templateArray = this.props.results_legbyleg.value;
            // }
            // else if (!isUndefined(this.props.results_legbyleg.delta)) {
            //     templateArray = this.props.results_legbyleg.delta;
            // }
            // else if (!isUndefined(this.props.results_legbyleg.gamma)) {
            //     templateArray = this.props.results_legbyleg.gamma;
            // }
            // else if (!isUndefined(this.props.results_legbyleg.vega)) {
            //     templateArray = this.props.results_legbyleg.vega;
            // }
            // else if (!isUndefined(this.props.results_legbyleg.theta)) {
            //     templateArray = this.props.results_legbyleg.theta;
            // }
            // else if (!isUndefined(this.props.results_legbyleg.rho)) {
            //     templateArray = this.props.results_legbyleg.rho;
            // }


            this.props.results_legbyleg.value.forEach((item, idx) => {
                legNum += 1;
                legResultsHtml.push(
                    <div className="leg-result-box" key={ idx }>
                        <h3>Contract { legNum }</h3>
                        <ul>
                            <li>
                                <span className="key">Value</span>
                                <FormattedNumber numDecimalPlaces={ NumDecimalPlaces }
                                                 otherClasses="single-value"
                                >
                                    { this._getValue(this.props.results_legbyleg.value, idx) }
                                </FormattedNumber>
                            </li>
                            <li><span className="key">Delta</span>
                                <FormattedNumber numDecimalPlaces={ NumDecimalPlaces }
                                                 otherClasses="single-value"
                                >
                                    { this._getValue(this.props.results_legbyleg.delta, idx) }
                                </FormattedNumber>
                            </li>
                            <li><span className="key">Gamma</span>
                                <FormattedNumber numDecimalPlaces={ NumDecimalPlaces }
                                                 otherClasses="single-value"
                                >
                                    { this._getValue(this.props.results_legbyleg.gamma, idx) }
                                </FormattedNumber>
                            </li>
                            <li><span className="key">Vega</span>
                                <FormattedNumber numDecimalPlaces={ NumDecimalPlaces }
                                                 otherClasses="single-value"
                                >
                                    { this._getValue(this.props.results_legbyleg.vega, idx) }
                                </FormattedNumber>
                            </li>
                            <li><span className="key">Theta</span>
                                <FormattedNumber numDecimalPlaces={ NumDecimalPlaces }
                                                 otherClasses="single-value"
                                >
                                    { this._getValue(this.props.results_legbyleg.theta, idx) }
                                </FormattedNumber>
                            </li>
                            <li><span className="key">Rho</span>
                                <FormattedNumber numDecimalPlaces={ NumDecimalPlaces }
                                                 otherClasses="single-value"
                                >
                                    { this._getValue(this.props.results_legbyleg.rho, idx) }
                                </FormattedNumber>
                            </li>
                        </ul>
                    </div>
                );
            });
        }
        if (process.env.NODE_ENV !== 'production') {
            console.log(legResultsHtml);
        }
        return legResultsHtml;
    }

    render() {
        const legByLegResultsHTML = this.resultsByLeg();

        var refreshMsg = '';
        if (this.props.refresh_results) {
            refreshMsg = 'Run the calculations to update results';
        }

        // Set the leg-by-leg result header if by-contract breakdown was requested
        var byLegHeaderHTML = '';
        if (this.props.LegByLegChecked) {
            byLegHeaderHTML = <h1>Contract Value and Greeks</h1>;
        }
        else {
            byLegHeaderHTML = '';
        }

        const messages = this.getMessagesForLabel(MessageLabels.PRICER);

        return (

            <section id="results">
                <h1>Value and Greeks</h1>
                <span className="message">{ refreshMsg }</span>

                <div className="messages">
                    <MessageList messages={ messages } />
                </div>

                <div id="deal-by-deal-chbx">
                    <label className="label-switch">
                        <span className="label-name">Split results by contract</span>
                        <input
                            type="checkbox"
                            onChange={ this._onChange }
                            checked={ this.props.LegByLegChecked }
                        />
                        <div className="checkbox"></div>
                    </label>
                </div>


                <section id="results-by-leg" className={this.toggleConfigClass('')}>
                    { byLegHeaderHTML }
                    { legByLegResultsHTML }
                    
                </section>
                <section id="results-strategy">
                    <h1>Strategy Value and Greeks</h1>
                    <ul>
                        <li><span className="key">Value</span>
                            <FormattedNumber numDecimalPlaces={ NumDecimalPlaces }
                                             otherClasses="total-value"
                            >
                                { this.props.results.value }
                            </FormattedNumber>
                        </li>

                        <li><span className="key">Delta</span>
                            <FormattedNumber numDecimalPlaces={ NumDecimalPlaces }
                                             otherClasses="total-value"
                            >
                                { this.props.results.delta }
                            </FormattedNumber>
                        </li>

                        <li><span className="key">Gamma</span>
                            <FormattedNumber numDecimalPlaces={ NumDecimalPlaces }
                                             otherClasses="total-value"
                            >
                                { this.props.results.gamma }
                            </FormattedNumber>
                        </li>

                        <li><span className="key">Vega</span>
                            <FormattedNumber numDecimalPlaces={ NumDecimalPlaces }
                                             otherClasses="total-value"
                            >
                                { this.props.results.vega }
                            </FormattedNumber>
                        </li>

                        <li><span className="key">Theta</span>
                            <FormattedNumber numDecimalPlaces={ NumDecimalPlaces }
                                             otherClasses="total-value"
                            >
                                { this.props.results.theta }
                            </FormattedNumber>
                        </li>

                        <li><span className="key">Rho</span>
                            <FormattedNumber numDecimalPlaces={ NumDecimalPlaces }
                                             otherClasses="total-value"
                            >
                                { this.props.results.rho }
                            </FormattedNumber>
                        </li>
                    </ul>
                </section>
            </section>
        );
    }
}

Results.propTypes = {
    results: React.PropTypes.object.isRequired,
    results_legbyleg: React.PropTypes.object.isRequired,
    numLegResults: React.PropTypes.number.isRequired,
    LegByLegChecked: React.PropTypes.bool.isRequired,
    refresh_results: React.PropTypes.bool.isRequired,
};


export default Results;
