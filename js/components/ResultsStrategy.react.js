import React from 'react';
import { NumDecimalPlaces } from '../constants/ResultsConstants.js';
import FormattedNumber from './FormattedNumber.react';
import MessageStore from '../stores/MessageStore';
import MessageList from './MessageList.react.js';
import { MessageLabels } from '../constants/MessageConstants';


class ResultsStrategy extends React.Component {
    constructor(props) {
        super(props);
        this.getMessagesForLabel = this.getMessagesForLabel.bind(this);
    }

    getMessagesForLabel(label) {
        return MessageStore.getMessagesForLabel(label);
    }

    render() {

        const messages = this.getMessagesForLabel(MessageLabels.PRICER);

        return (

            <section id="results">
                <section id="results-strategy">
                    <h1>Strategy Value and Greeks</h1>

                    <div className="messages">
                        <MessageList messages={ messages } />
                    </div>

                    <span className="fa fa-fw fa-3x fa-calculator small-icon"></span>
                    <span className="fa fa-fw fa-3x fa-arrow-right small-icon"></span>
                    <ul>
                        <li>
                            <span className="key">Value</span>
                            <FormattedNumber
                                numDecimalPlaces={ NumDecimalPlaces }
                                otherClasses="total-value"
                            >
                                { this.props.results.value }
                            </FormattedNumber>
                        </li>

                        <li>
                            <span className="key">Delta</span>
                            <FormattedNumber
                                numDecimalPlaces={ NumDecimalPlaces }
                                otherClasses="total-value"
                            >
                                { this.props.results.delta }
                            </FormattedNumber>
                        </li>

                        <li>
                            <span className="key">Gamma</span>
                            <FormattedNumber
                                numDecimalPlaces={ NumDecimalPlaces }
                                otherClasses="total-value"
                            >
                                { this.props.results.gamma }
                            </FormattedNumber>
                        </li>

                        <li>
                            <span className="key">Vega</span>
                            <FormattedNumber
                                numDecimalPlaces={ NumDecimalPlaces }
                                otherClasses="total-value"
                            >
                                { this.props.results.vega }
                            </FormattedNumber>
                        </li>

                        <li>
                            <span className="key">Theta</span>
                            <FormattedNumber
                                numDecimalPlaces={ NumDecimalPlaces }
                                otherClasses="total-value"
                            >
                                { this.props.results.theta }
                            </FormattedNumber>
                        </li>

                        <li>
                            <span className="key">Rho</span>
                            <FormattedNumber
                                numDecimalPlaces={ NumDecimalPlaces }
                                otherClasses="total-value"
                            >
                                { this.props.results.rho }
                            </FormattedNumber>
                        </li>
                    </ul>
                </section>
            </section>
        );
    }
}

ResultsStrategy.propTypes = {
    results: React.PropTypes.object.isRequired,
};


export default ResultsStrategy;
