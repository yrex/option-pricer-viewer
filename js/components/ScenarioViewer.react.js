import React from 'react';
import ScenarioConfig from './ScenarioConfig.react';
import ScenarioGraph from './ScenarioGraph.react';
import ScenarioConfigStoreActions from '../actions/ScenarioConfigStoreActions';
import classnames from 'classnames';
import MessageList from './MessageList.react.js';
import MessageStore from '../stores/MessageStore';
import { MessageLabels } from '../constants/MessageConstants';
import { MAX_VIEW_WIDTH } from '../constants/StyleConstants';
import debounce from 'lodash.debounce';


class ScenarioViewer extends React.Component {

    constructor(props) {
        super(props);

        this.removeScenario = this.removeScenario.bind(this);
        this.toggleConfig = this.toggleConfig.bind(this);
        this.toggleConfigClass = this.toggleConfigClass.bind(this);
        this.getMessagesForLabel = this.getMessagesForLabel.bind(this);
        this._handleWindowResize = debounce(this._handleWindowResize.bind(this), 100);
        this._isViewerSmall = this._isViewerSmall.bind(this);
        this.toggleViewerClass = this.toggleViewerClass.bind(this);

        this.state = {
            toggleConfigOn: false,
            isViewerSmall: this._isViewerSmall(),
        };
    }

    componentDidMount() {
        window.addEventListener('resize', this._handleWindowResize);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this._handleWindowResize);
    }

    _handleWindowResize() {
        this.setState({
            isViewerSmall: this._isViewerSmall(),
        });
    }

    /**
     * Check if the viewer will be too small for the given window width.
     * @returns {boolean} - True if window is too small, otherwise false.
     * @private
     */
    _isViewerSmall() {
        return (window.innerWidth < (MAX_VIEW_WIDTH * 0.75));
    }

    toggleConfig() {
        this.setState((prevState) => ({
            toggleConfigOn: !prevState.toggleConfigOn,
            })
        );
    }

    toggleViewerClass(name) {
        return classnames(name, {
            'scenario-1-col': this.state.isViewerSmall,
            'scenario-2-col': !this.state.isViewerSmall,
        });
    }

    toggleConfigClass(name) {
        return classnames(name, {
            // 'scenario-subheader': true,
            'config-enabled': this.state.toggleConfigOn,
            'config-disabled': !this.state.toggleConfigOn,
        });
    }

    removeScenario() {
        // As we don't require this scenario anymore, reset the validation state to true,
        // i.e. even if the scenario had validation errors.
        //this.props.logValidationState(this.props.name, true);
        ScenarioConfigStoreActions.remove(this.props.id);
    }

    /**
     *
     * @param {string} label - Message label being requested
     * @param {number} id - Extra label used for filtering. Here this corresponds to the graph ID
     * @returns {Array} An array of messages is returned
     */
    getMessagesForLabel(label, id) {
        var allMessages = MessageStore.getMessagesForLabel(label);
        var messagesForId = [];
        allMessages.forEach((message) => {
            if (message.label.id === id) {
                messagesForId.push(message);
            }
        });
        return messagesForId;
    }

    render() {
        if (process.env.NODE_ENV !== 'production') {
            console.log(this.props.scenarioConfig);
        }

        // the delete button is permanently enabled
        const btnEnabled = true;

        const messages = this.getMessagesForLabel(MessageLabels.SCENARIO, this.props.id);

        return (
            <div className={this.toggleViewerClass('scenario-viewer')}>
                <div className="scenario-header">
                    <h3>Profile { this.props.scenarioNum }
                        { /* , id: { this.props.id } */ }
                    </h3>

                    <button
                        type="button"
                        className={ btnEnabled ? 'enabled' : 'disabled' }
                        onClick= { this.removeScenario }
                        disabled= { !btnEnabled }
                        title="Delete Profile"
                    >
                        <span className="fa fa-fw fa-times-circle"></span><span className="info">Delete Profile</span>
                    </button>


                    <button
                        type="button"
                        className = { this.toggleConfigClass('config-toggle-button') }
                        onClick= { this.toggleConfig }
                        title="Configure Profile"
                    >
                        <span className="fa fa-fw fa-cog"></span><span className="info">Configuration</span>
                    </button>

                    <div className="messages">
                        <MessageList messages={ messages } />
                    </div>

                    <div className={this.toggleConfigClass('scenario-subheader')}>


                        <ScenarioConfig
                            key={ this.props.id }
                            id={ this.props.id }
                            label={ this.props.scenarioNum }
                            params={ this.props.scenarioConfig }
                        />
                    </div>
                </div>

                <ScenarioGraph
                    key={ 'graph-' + this.props.id }
                    params={ this.props.graphParams }
                    xLabel={ this.props.scenarioConfig.x_type }
                    yLabel={ this.props.scenarioConfig.y_type }
                />

            </div>
        );
    }
}

ScenarioViewer.PropTypes = {
    id: React.PropTypes.number.isRequired,
    scenarioNum: React.PropTypes.number.isRequired,
    scenarioConfig: React.PropTypes.object.isRequired,
    graphParams: React.PropTypes.object.isRequired,
    name: React.PropTypes.string.isRequired,
};

export default ScenarioViewer;
