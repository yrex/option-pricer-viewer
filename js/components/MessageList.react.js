import React from 'react';
import Message from './Message.react';
import MessageStoreActions from '../actions/MessageStoreActions';
import { countItems } from '../common/utils';


class MessageList extends React.Component {
    constructor(props) {
        super(props);
        this.removeMessage = this.removeMessage.bind(this);
    }

    removeMessage(id) {
        MessageStoreActions.DeleteMessage(id);
    }

    render() {
        const messageList = this.props.messages.map((m, idx) => {
            return (
                <Message
                    key={ idx }
                    id={ idx }
                    message={ m }
                    onItemClick={ this.removeMessage }
                />
            );
        });

        // output the <ul> only if messages are available
        if (countItems(messageList) !== 0) {
            return (

                <ul className="message-list">
                    { messageList }
                </ul>

            );
        }
        else {
            return (null);
        }
    }

}

MessageList.propTypes = {
    messages: React.PropTypes.array.isRequired,
};

export default MessageList;
