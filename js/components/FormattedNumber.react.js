import React from 'react';
import classNames from 'classnames';
import { formatNumber, isNumeric } from '../common/utils';


class FormattedNumber extends React.Component {
    constructor(props) {
        super(props);
        this.getClasses = this.getClasses.bind(this);
    }

    /**
     * Construct the class names for the <span> based on
     *  1) the given default class names (a string or array of strings)
     *  2) the sign of the input value
     * @param {number} inputValue - The input value, a number
     * @param {string | string[]} [defaultClasses] - Default classes that should be appended
     */
    getClasses(inputValue, defaultClasses) {
        // empty, null or undefined values in defaultClasses are ignored by the classnames() method
        // check if the input is a number. If it is, add a class based on the sign of the number
        if (isNumeric(inputValue)) {
            return classNames(
                defaultClasses,
                { 'negative-value': inputValue < 0 }
            );
        }
        // otherwise return with default classes
        return classNames(defaultClasses);
    }

    render() {
        const value = this.props.children;
        const otherClasses = this.props.otherClasses || null;
        return (
            <span className={ this.getClasses(value, otherClasses) }>
                { formatNumber(value, this.props.numDecimalPlaces) }
            </span>
        );
    }

}

FormattedNumber.propTypes = {
    children: React.PropTypes.number.isRequired,
    numDecimalPlaces: React.PropTypes.number.isRequired,
    otherClasses: React.PropTypes.oneOfType([
        React.PropTypes.string,
        React.PropTypes.array,
    ]),
};

export default FormattedNumber;
