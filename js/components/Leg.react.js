import React from 'react';
import LegStoreActions from '../actions/LegActions';
import LegStore from '../stores/LegStore';
import Joi from 'joi-browser';
import validation from 'react-validation-mixin';
import ValidationStrategy from 'joi-browser-validation-strategy';
import classnames from 'classnames';
import { isEmpty } from '../common/utils';


class Leg extends React.Component {

    constructor(props) {
        super(props);

        this.validatorTypes = {
            price: Joi.number().min(0).max(100000).required().label('Price'),
            strike: Joi.number().min(0).max(100000).required().label('Strike'),
            vol: Joi.number().min(0).max(100).required().label('Volatility'),
            rate: Joi.number().min(0).max(100).required().label('rate'),
            time: Joi.number().min(0).max(40).required().label('Time To Expiry'),
            dividend: Joi.number().min(0).max(100).required().label('Dividend Yield'),
        };

        this.getValidatorData = this.getValidatorData.bind(this);
        this.renderHelpText = this.renderHelpText.bind(this);
        this.getClasses = this.getClasses.bind(this);
        this._onChange = this._onChange.bind(this);
        this.removeLeg = this.removeLeg.bind(this);
        this.getFieldId = this.getFieldId.bind(this);
    }

    getValidatorData() {
        return this.props.params;
    }

    getClasses(field) {
        return classnames({
            'form-group': true,
            'has-error': !this.props.isValid(field),
        });
    }

    getFieldId(prefix) {
        return prefix + '-' + this.props.id;
    }

    removeLeg() {
        // As we don't require this leg anymore, reset the validation state to true,
        // i.e. even if the leg had validation errors.
        this.props.logValidationState(this.props.name, true);
        LegStoreActions.remove(this.props.id);
    }

    _onChange(fieldname) {
        return event => {
            const value = event.target.value;
            LegStoreActions.update(this.props.id, fieldname, value);
        };
    }

    handleValidationCallback() {
        return () => {
            this.props.logValidationState(this.props.name, isEmpty(this.props.errors));
        };
    }

    renderHelpText(message) {
        return (
            <span className="help-block">{message}</span>
        );
    }

    render() {
        const btnEnabled = LegStore.getNumLegs() > 1;


        return (

            <fieldset className="leg">
                <legend>Contract { this.props.legNum + ' -- ' + this.props.id }</legend>

                <span className="fieldset-header">
                    <span className="caption">
                        Contract { this.props.legNum + ' -- ' + this.props.id }
                    </span>
                    <button
                        type="button"
                        className={ btnEnabled ? 'enabled' : 'disabled' }
                        onClick= { this.removeLeg }
                        disabled= { !btnEnabled }
                    >
                    <span className="fa fa-fw fa-times"></span><span className="info">Delete Scenario</span>
                    </button>
                </span>

                <div className={this.getClasses('buysell')}>
                    <label htmlFor={this.getFieldId('buysell')}>Buy/Sell</label>
                    <select
                        id={this.getFieldId('buysell')}
                        value={ this.props.params.buysell }
                        onChange={ this._onChange('buysell') }
                    >
                        <option value="buy">Buy</option>
                        <option value="sell">Sell</option>
                    </select>
                </div>

                <div className={this.getClasses('callput')}>
                    <label htmlFor={this.getFieldId('callput')}>Call/Put</label>
                    <select
                        id={this.getFieldId('callput')}
                        value={ this.props.params.callput }
                        onChange={ this._onChange('callput') }
                    >
                        <option value="call">Call</option>
                        <option value="put">Put</option>
                    </select>
                </div>

                <div className={this.getClasses('price')}>
                    <label htmlFor={this.getFieldId('price')}>Price</label>
                    <input
                        id={this.getFieldId('price')}
                        value={ this.props.params.price }
                        type="text"
                        placeholder="0"
                        onChange={ this._onChange('price') }
                        onBlur={ this.props.handleValidation('price', this.handleValidationCallback()) }
                    />
                    {this.renderHelpText(this.props.getValidationMessages('price'))}
                </div>


                <div className={this.getClasses('strike')}>
                    <label htmlFor={this.getFieldId('strike')}>Strike</label>
                    <input
                        id={this.getFieldId('strike')}
                        value={ this.props.params.strike }
                        type="text"
                        placeholder="0"
                        onChange={ this._onChange('strike') }
                        onBlur={ this.props.handleValidation('strike', this.handleValidationCallback()) }
                    />
                    {this.renderHelpText(this.props.getValidationMessages('strike'))}
                </div>


                <div className={this.getClasses('vol')}>
                    <label htmlFor={this.getFieldId('vol')}>Vol (%)</label>
                    <input
                        id={this.getFieldId('vol')}
                        value={ this.props.params.vol }
                        type="text"
                        placeholder="0"
                        onChange={ this._onChange('vol') }
                        onBlur={ this.props.handleValidation('vol', this.handleValidationCallback()) }
                    />
                    {this.renderHelpText(this.props.getValidationMessages('vol'))}
                </div>


                <div className={this.getClasses('rate')}>
                    <label htmlFor={this.getFieldId('rate')}>Rate (%)</label>
                    <input
                        id={this.getFieldId('rate')}
                        value={ this.props.params.rate }
                        type="text"
                        placeholder="0"
                        onChange={ this._onChange('rate') }
                        onBlur={ this.props.handleValidation('rate', this.handleValidationCallback()) }
                    />
                    {this.renderHelpText(this.props.getValidationMessages('rate'))}
                </div>


                <div className={this.getClasses('time')}>
                    <label htmlFor={this.getFieldId('time')}>Time to Expiry</label>
                    <input
                        id={this.getFieldId('time')}
                        value={ this.props.params.time }
                        type="text"
                        placeholder="0"
                        onChange={ this._onChange('time') }
                        onBlur={ this.props.handleValidation(
                                    'time', this.handleValidationCallback()) }
                    />
                    {this.renderHelpText(this.props.getValidationMessages('time'))}
                </div>


                <div className={this.getClasses('dividend')}>
                    <label htmlFor={this.getFieldId('dividend')}>Dividend</label>
                    <input
                        id={this.getFieldId('dividend')}
                        value={ this.props.params.dividend }
                        type="text"
                        placeholder="0"
                        onChange={ this._onChange('dividend') }
                        onBlur={ this.props.handleValidation(
                                    'dividend', this.handleValidationCallback()) }
                    />
                    {this.renderHelpText(this.props.getValidationMessages('dividend'))}
                </div>

            </fieldset>

        );
    }
}

Leg.propTypes = {
    id: React.PropTypes.number.isRequired,
    name: React.PropTypes.string.isRequired,
    legNum: React.PropTypes.number.isRequired,
    params: React.PropTypes.object.isRequired,
    logValidationState: React.PropTypes.func.isRequired,
    errors: React.PropTypes.object,
    validate: React.PropTypes.func,
    isValid: React.PropTypes.func,
    handleValidation: React.PropTypes.func,
    getValidationMessages: React.PropTypes.func,
    clearValidations: React.PropTypes.func,
};

export default validation(ValidationStrategy)(Leg);
