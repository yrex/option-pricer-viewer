import React from 'react';
import { NumDecimalPlaces } from '../constants/ResultsConstants.js';
import FormattedNumber from './FormattedNumber.react';


class ResultLeg extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="leg-result-box" key={ this.props.id }>
                <h3>
                    Contract { this.props.legNum }
                    { /* , ID: { this.props.id }*/ }
                    </h3>
                <ul>
                    <li>
                        <span className="key">Value</span>
                        <FormattedNumber
                            numDecimalPlaces={ NumDecimalPlaces }
                            otherClasses="single-value"
                        >
                            { this.props.results.value }
                        </FormattedNumber>
                    </li>
                    <li><span className="key">Delta</span>
                        <FormattedNumber
                            numDecimalPlaces={ NumDecimalPlaces }
                            otherClasses="single-value"
                        >
                            { this.props.results.delta }
                        </FormattedNumber>
                    </li>
                    <li><span className="key">Gamma</span>
                        <FormattedNumber
                            numDecimalPlaces={ NumDecimalPlaces }
                            otherClasses="single-value"
                        >
                            { this.props.results.gamma }
                        </FormattedNumber>
                    </li>
                    <li><span className="key">Vega</span>
                        <FormattedNumber
                            numDecimalPlaces={ NumDecimalPlaces }
                            otherClasses="single-value"
                        >
                            { this.props.results.vega }
                        </FormattedNumber>
                    </li>
                    <li><span className="key">Theta</span>
                        <FormattedNumber
                            numDecimalPlaces={ NumDecimalPlaces }
                            otherClasses="single-value"
                        >
                            { this.props.results.theta }
                        </FormattedNumber>
                    </li>
                    <li><span className="key">Rho</span>
                        <FormattedNumber
                            numDecimalPlaces={ NumDecimalPlaces }
                            otherClasses="single-value"
                        >
                            { this.props.results.rho }
                        </FormattedNumber>
                    </li>
                </ul>
            </div>
        );
    }

}

ResultLeg.propTypes = {
    id: React.PropTypes.number.isRequired,
    results: React.PropTypes.object.isRequired,
    legNum: React.PropTypes.number.isRequired,
};


export default ResultLeg;
