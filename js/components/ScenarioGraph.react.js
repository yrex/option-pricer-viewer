import React from 'react';
import { isArrayEqual } from '../common/utils';
import { Chart } from 'react-google-charts';
import { GOOGLE_CHART_VERSION } from '../constants/GoogleChartConstants';
import { Scenario_xVals, Scenario_yVals } from '../constants/ScenarioConfigConstants';
import { MAX_VIEW_WIDTH, DEFAULT_CHART_HEIGHT } from '../constants/StyleConstants';
import debounce from 'lodash.debounce';


class ScenarioGraphChart extends React.Component {

    constructor(props) {
        super(props);
        this.state = this.getItemState();

        this.makeDatasets = this.makeDatasets.bind(this);
        this.getItemState = this.getItemState.bind(this);
        this.toggleLegByLeg = this.toggleLegByLeg.bind(this);
        this.getAxisName = this.getAxisName.bind(this);
        this.shouldComponentUpdate = this.shouldComponentUpdate.bind(this);
        this._handleWindowResize = this._handleWindowResize.bind(this);
        this._windowSize = debounce(this._windowSize.bind(this), 100);
    }

    componentDidMount() {
        window.addEventListener('resize', this._handleWindowResize);
    }

    /**
     * This React method is used to work around the chart js limitation of updating graphs.
     * Once a given number of lines are added to a graph, it is not possible to add/reduce
     * the number of lines. So, we need to always enable the 'redraw' prop.
     * To avoid unnecessary recalculations of the components, we use React's method to check.
     *
     * @param nextProps
     * @param nextState
     * @returns {boolean}
     */
    shouldComponentUpdate(nextProps, nextState) {
        if (process.env.NODE_ENV !== 'production') {
            console.log('Should graph update?');
        }

        // if the leg-by-leg graph has been toggled, update the component
        if (this.state.displayLegByLeg !== nextState.displayLegByLeg) {
            return true;
        }

        // if the window has been resized, update the component
        if (this.state.windowSize !== nextState.windowSize) {
            return true;
        }

        // if the inputs haven't changed, don't update the component
        if (isArrayEqual(this.props.params.xVals, nextProps.params.xVals) &&
            isArrayEqual(this.props.params.yValByLeg, nextProps.params.yValByLeg)) {
            if (process.env.NODE_ENV !== 'production') {
                console.log('... no it shouldn\'t!');
            }
            return false;
        }

        // otherwise the component should be updated
        if (process.env.NODE_ENV !== 'production') {
            console.log('yes it should!');
        }
        return true;
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this._handleWindowResize);
    }

    _handleWindowResize() {
        this.setState({
            windowSize: this._windowSize(),
        });
    }

    /**
     * Method to retrieve all state parameters
     * @returns object of state parameters
     */
    getItemState() {
        // note:
        //   windowSize - we are interested in capturing the actual window size when
        //                it is smaller than the maximum grid size.
        return {
            displayLegByLeg: false,
            windowSize: this._windowSize(),
        };
    }

    _windowSize() {
        return Math.min(window.innerWidth, MAX_VIEW_WIDTH);
    }

    toggleLegByLeg() {
        this.setState({ displayLegByLeg: !this.state.displayLegByLeg });
    }

    makeDatasets() {
        var rows = [];
        var columns = [];

        var j;
        const numPoints = this.props.params.yValByLeg.length;

        // Set up the data rows
        // if no data point are available, set the value to (0,0)
        if (numPoints === 0) {
            rows = [[0, 0]];
        }
        for (j = 0; j < numPoints; j++) {
            // if leg by leg output is requested
            if (this.state.displayLegByLeg && this.props.params.yValByLeg.length) {
                var tempArr = [];
                tempArr.push(this.props.params.xVals[j]);
                tempArr.push(this.props.params.yValSummary[j]);
                tempArr.push.apply(tempArr, this.props.params.yValByLeg[j]);
                rows.push(tempArr);
            }
            else {
                rows.push([
                    this.props.params.xVals[j],
                    this.props.params.yValSummary[j],
                ]);
            }
        }

        // Set up the data columns
        columns.push({
            type: 'number',
            label: 'X',
        });

        columns.push({
            type: 'number',
            label: 'Deal',
        });

        if (this.state.displayLegByLeg && this.props.params.yValByLeg.length) {
            const numLegs = this.props.params.yValByLeg[0].length;
            for (j=0; j < numLegs; j++) {
                columns.push({
                    type: 'number',
                    label: 'Contract ' + (j + 1),
                });
            }
        }

        // returns rows and columns as object
        return { rows, columns };
    }

    /**
     * Get the name of a scenario variable based on its value. This is to retrieve the
     * xVal and yVal names from the Scenario_xVals and Scenario_yVals constants.
     * @param {String} value - the value being looked up
     * @param {Array} group - The array of objects containing the items. Either of Scenario_xVals or Scenario_yVals.
     */
    getAxisName(value, group) {
        var i;
        for (i = 0; i < group.length; i++) {
            if (group[i].value === value) {
                return group[i].name;
            }
        }
    }

    render() {
        // get rows and columns
        // object deconstruct
        var { rows, columns } = this.makeDatasets();
        if (process.env.NODE_ENV !== 'production') {
            console.log(rows);
            console.log(columns);
        }

        const xAxis = this.getAxisName(this.props.xLabel, Scenario_xVals);
        const yAxis = this.getAxisName(this.props.yLabel, Scenario_yVals);
        const graphTitle = xAxis + ' vs ' + yAxis;
        const chartHeight = DEFAULT_CHART_HEIGHT * this.state.windowSize / MAX_VIEW_WIDTH;

        // get options
        var options = {
            // title: graphTitle,
            hAxis: {
                title: this.getAxisName(this.props.xLabel, Scenario_xVals),
                titleTextStyle: {
                    italic: false,
                },
            },
            vAxis: {
                title: this.getAxisName(this.props.yLabel, Scenario_yVals),
                titleTextStyle: {
                    italic: false,
                },
            },
            chartArea: {
                top: '5%',
                left: '10%',
                width: '100%',
                // backgroundColor: '#555',
            },
            legend: {
                position: 'bottom',
            },
            tooltip: {
                showColorCode: true,
            },
            series: {
                // distinguish between the summary values and leg values
                0: {
                    color: '#333',
                    lineWidth: 4,
                },
            },
        };

        return (
            <div className="scenario-viewer-graph-box">

                <h4 className="graph-title">{ xAxis } vs. { yAxis }</h4>

                <div className="chart-outer">
                    <Chart
                        width={ "90%" }
                        height={ chartHeight + 'px' }
                        legend_toggle={ true }
                        rows={ rows }
                        columns={ columns }
                        options={ options }
                        chartVersion={ GOOGLE_CHART_VERSION }
                    />
                </div>

                <div className="deal-by-deal-graph">
                    <label className="label-switch">
                        <span className="label-name">Display profile by contract</span>
                        <input
                            type="checkbox"
                            onChange={ this.toggleLegByLeg }
                            checked={ this.state.displayLegByLeg }
                        />
                        <div className="checkbox"></div>
                    </label>
                </div>

            </div>
        );
    }
}

ScenarioGraphChart.PropTypes = {
    params: React.PropTypes.object.isRequired,
    xLabel: React.PropTypes.string.isRequired,
    ylabel: React.PropTypes.string.isRequired,
};

export default ScenarioGraphChart;
