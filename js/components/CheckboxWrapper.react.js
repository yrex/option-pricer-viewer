import React from 'react';


class CheckBoxWrapper extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <label>
                <input
                    type="checkbox"
                    value={ this.props.value }
                    checked={ this.props.checked }
                    onChange={ this.props.onChangeHandler }
                />
                { this.props.label }
            </label>
        );
    }
}

CheckBoxWrapper.PropTypes = {
    value: React.PropTypes.string.isRequired,
    checked: React.PropTypes.bool.isRequired,
    onChangeHandler: React.PropTypes.func.isRequired,
    label: React.PropTypes.string.isRequired,
};


export default CheckBoxWrapper;
