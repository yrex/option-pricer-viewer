import React from 'react/lib/ReactIsomorphic.js';
import LegStore from '../stores/LegStore';
import LegStoreActions from '../actions/LegActions';
import StrategyListStoreActions from '../actions/StrategyListStoreActions';


class StrategyList extends React.Component {

    constructor(props) {
        super(props);
        this.state = this.getItemState();

        this.getItemState = this.getItemState.bind(this);
        this.showStrategyMenu = this.showStrategyMenu.bind(this);
        this._onClick = this._onClick.bind(this);
        this._onInit = this._onInit.bind(this);
    }

    componentDidMount() {
        LegStore.addInitListener(this._onInit);
    }

    componentWillUnmount() {
        LegStore.removeInitListener(this._onInit);
    }

    _onInit() {
        this.setState(this.getItemState());
    }

    _onClick(value) {
        return () => {
            LegStoreActions.setLegPreset(value);
        };
    }

    showStrategyMenu() {
        StrategyListStoreActions.toggleDisplay();
    }

    getItemState() {
        return {
            strategyItems: LegStore.getStrategyList(),
        };
    }

    render() {
        const items = this.state.strategyItems;

        const stratList = Object.keys(items).map( (d, idx) => {
            return (
                <li key={ idx }
                    onClick= { this._onClick(d) }
                >
                    <span>
                        { items[d].name }
                    </span>
                </li>
            );
        });

        return (
            <div className="strategy-bar-wrapper">
                <h2>Strategies</h2>
                <p><span className="fa fa-fw fa-angle-double-right"></span>
                    Choose from one of the following common strategies. alternatively, build your own custom strategy.
                </p>
                <ul>
                    { stratList }
                </ul>

                <button
                    type="button"
                    onClick= { this.showStrategyMenu }
                >
                    <span className="fa fa-fw fa-times"></span><span className="info">Close menu</span>
                </button>

            </div>
        );
    }
}

export default StrategyList;
