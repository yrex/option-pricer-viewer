import React from 'react';


class Message extends React.Component {
    constructor(props) {
        super(props);
        this._onClick = this._onClick.bind(this);
    }

    _onClick(messageID) {
        // this.props.onItemClick(this.props.id);
        return () => {
            this.props.onItemClick(messageID);
        };
    }

    render() {
        const m = this.props.message;
        return (
            <li className={ m.type.toLowerCase() }>
                <span className="text">{ m.message }</span>
                <div className="button-wrapper">
                    <button
                        type="button"
                        className="message-button"
                        onClick={ this._onClick(m.index) }
                        title="dismiss"
                    >
                        <span className="fa fa-fw fa-minus-circle"></span>
                        <span className="info">Dismiss</span>
                    </button>
                </div>
            </li>
        )}

}

Message.propTypes = {
    id: React.PropTypes.number.isRequired,
    onItemClick: React.PropTypes.func.isRequired,
    message: React.PropTypes.object.isRequired,
};

export default Message;
