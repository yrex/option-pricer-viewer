import React from 'react';
import ScenarioConfigStoreActions from '../actions/ScenarioConfigStoreActions';
import ScenarioViewer from './ScenarioViewer.react';
import ScenarioConfigStore from '../stores/ScenarioConfigStore';
import MessageStore from '../stores/MessageStore';
import { ScenarioGraphStore } from '../stores/ScenarioGraphStore';


class Scenarios extends React.Component {

    constructor(props) {
        super(props);

        this.state = this.getItemState();

        this.AddScenario = this.AddScenario.bind(this);
        this._onChange = this._onChange.bind(this);
        this.getItemState = this.getItemState.bind(this);
    }

    componentDidMount() {
        ScenarioConfigStore.addChangeListener(this._onChange);
        ScenarioGraphStore.addChangeListener(this._onChange);
        MessageStore.addChangeListener(this._onChange);
    }

    componentWillUnmount() {
        ScenarioConfigStore.removeChangeListener(this._onChange);
        ScenarioGraphStore.removeChangeListener(this._onChange);
        MessageStore.removeChangeListener(this._onChange);
    }

    _onChange() {
        this.setState(this.getItemState());
    }

    /**
     * Method to retrieve all state parameters
     * @returns object of state parameters
     */
    getItemState() {
        return {
            scenarioConfig: ScenarioConfigStore.getAllScenarios(),
            scenarioGraphs: ScenarioGraphStore.getAll(),
        };
    }

    /**
     * Add a scenario. Call an Action to append an an item to the ScenarioConfigStore.
     * @param e {object} event object.
     */
    AddScenario(e) {
        e.preventDefault();
        ScenarioConfigStoreActions.add();
    }

    render() {
        const scenarioConfigs = this.state.scenarioConfig;
        const graphParams = this.state.scenarioGraphs;

        const scenarioHtml = [];
        var scenarioNum = 0;
        const logValidationState = this.props.logValidationState;

        scenarioConfigs.forEach((config, idx) => {
            scenarioNum += 1;
            scenarioHtml.push(
                <ScenarioViewer
                    key={ idx }
                    id={ idx }
                    scenarioNum={ scenarioNum }
                    scenarioConfig={ config }
                    graphParams={ graphParams[idx] }
                    name={'scenario' + idx }
                    logValidationState={ logValidationState }
                />
            );
        });

        if (process.env.NODE_ENV !== 'production') {
            console.log('config: ', scenarioConfigs);
            console.log('graph: ', graphParams);
        }


        return (

            <div className="scenarios">

                <header className="scenario-intro">
                    <h3>Strategy Profile</h3>
                    <p>Once you have defined your strategy, create different profiles to understand
                        its behaviour.<br/>
                        Start by adjusting the profile below. Click the <span className="fa fa-fw fa-cog"></span> icon
                        to set the input, then click on the Update Scenario button.<br/>
                        You can add multiple profiles by clicking the Add Profile button.<br/>
                        <span className="fa fa-fw fa-lightbulb-o"></span> Your profiles will automatically
                        update when you change your strategy and click the Run Calculations button.
                    </p>

                    <button type="button"
                            onClick={ this.AddScenario }
                            className="add-button"
                    >
                        <span className="fa fa-fw fa-plus-circle"></span>
                        <span className="button-text">Add Profile</span>
                    </button>

                </header>

                { scenarioHtml }

            </div>
        );
    }
}


export default Scenarios;
