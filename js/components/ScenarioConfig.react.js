import React from 'react';
import Joi from 'joi-browser';
import validation from 'react-validation-mixin';
import ValidationStrategy from 'joi-browser-validation-strategy';
import classnames from 'classnames';
import ScenarioConfigStoreActions from '../actions/ScenarioConfigStoreActions';
import { Scenario_xVals, Scenario_yVals } from '../constants/ScenarioConfigConstants';
import { BSParams } from '../constants/APIConstants';
import { ScenarioGraphActionCreator } from '../actions/ScenarioGraphStoreActions';


class ScenarioConfig extends React.Component {

    constructor(props) {
        super(props);

        // validation definitions
        this.validatorTypes = {
            x_max: Joi.number()
                      .when('x_type', { is: BSParams.SIGMA,
                                        then: Joi.number().min(0).max(100).required() })
                      .when('x_type', { is: BSParams.T,
                                        then: Joi.number().min(0).max(40).required() })
                      .when('x_type', { is: BSParams.R,
                                        then: Joi.number().min(0).max(100).required(),
                                        otherwise: Joi.number().min(0).max(100000).required() })
                      .label('Max x'),
            x_min: Joi.number().less(Joi.ref('x_max'))
                      .when('x_type', { is: BSParams.SIGMA,
                                        then: Joi.number().min(0).max(100).required() })
                      .when('x_type', { is: BSParams.T,
                                        then: Joi.number().min(0).max(40).required() })
                      .when('x_type', { is: BSParams.R,
                                        then: Joi.number().min(0).max(100).required(),
                                        otherwise: Joi.number().min(0).max(100000).required() })
                      .label('Min x'),
            num_points: Joi.number().min(1).max(30).required().label('Number of values'),
            time_offset: Joi.number().min(0).max(40).required().label('Elapsed time'),
        };

        this._onSubmit = this._onSubmit.bind(this);
        this._onKeyPress = this._onKeyPress.bind(this);
        this._onChange = this._onChange.bind(this);
        this.getValidatorData = this.getValidatorData.bind(this);
        this.renderHelpText = this.renderHelpText.bind(this);
        this.getClasses = this.getClasses.bind(this);
        this.getFieldId = this.getFieldId.bind(this);
        this.makeOptions = this.makeOptions.bind(this);
        this.handleValidationCallback = this.handleValidationCallback.bind(this);
    }

    _onSubmit(e) {
        e.preventDefault();
        ScenarioGraphActionCreator.APIGet(this.props.id, this.props.label);
        //console.log("I can't submit yet:", this.props.id);
    }

    _onKeyPress(e) {
        // disable form submit on enter key press
        if (e.key === 'Enter') {
            if (process.env.NODE_ENV !== 'production') {
                console.log('No enter key!');
            }
            e.preventDefault();
        }
    }

    _onChange(fieldname, callback) {
        return event => {
            const value = event.target.value;
            if (process.env.NODE_ENV !== 'production') {
                console.log(value);
            }
            ScenarioConfigStoreActions.update(this.props.id, fieldname, value);

            if (typeof callback === 'function') {
                callback();
            }
        };
    }

    getFieldId(prefix) {
        return prefix + '-' + this.props.id;
    }

    getValidatorData() {
        return this.props.params;
    }

    handleValidationCallback() {
        // This component has interdependent validation rules, so we will force
        // a full validation of all data
        return () => this.props.validate((err) => {
            if (err) {
                if (process.env.NODE_ENV !== 'production') {
                    console.log('Oops, error!');
                }
                // If the validation state has changed, update the store
                if (this.props.params.enabled) {
                    if (process.env.NODE_ENV !== 'production') {
                        console.log('Oops, error! -- triggered');
                    }
                    ScenarioConfigStoreActions.updateValidation(this.props.id, false);
                }
            }
            else {
                if (process.env.NODE_ENV !== 'production') {
                    console.log('No Errors!');
                }
                // If the validation state has changed, update the store
                if (!this.props.params.enabled) {
                    if (process.env.NODE_ENV !== 'production') {
                        console.log('No Errors! -- triggered!');
                    }
                    ScenarioConfigStoreActions.updateValidation(this.props.id, true);
                }
            }
        });
    }

    renderHelpText(message) {
        return (
            <span className="help-block">{message}</span>
        );
    }

    getClasses(field) {
        return classnames(field, {
            'form-group': true,
            'has-error': !this.props.isValid(field),
        });
    }

    /**
     * Create <select> elements using an array of name/value objects.
     * @param {Array} items This is an array of objects which contain a name and value
     * @returns {Array} An array of <select> elements
     */
    makeOptions(items) {
        var choices = [];
        items.forEach((item, idx) => {
            choices.push(
                <option key={ idx } value={ item.value }>
                    { item.name }
                </option>
            );
        });

        return choices;
    }

    render() {

        const calcEnabled = this.props.params.enabled && this.props.isValid();


        return (
            <div className="scenario-viewer-config">

                <form
                    onSubmit={this._onSubmit}
                    onKeyPress={this._onKeyPress}
                >

                    <div className={this.getClasses('x_type')}>
                        <label htmlFor={this.getFieldId('x_type')}>X-Axis</label>
                        <select
                            id={this.getFieldId('x_type')}
                            value={ this.props.params.x_type }
                            onChange={ this._onChange('x_type', this.handleValidationCallback()) }
                        >
                            { this.makeOptions(Scenario_xVals) }
                        </select>
                    </div>

                    <div className={this.getClasses('y_type')}>
                        <label htmlFor={this.getFieldId('y_type')}>Y-Axis</label>
                        <select
                            id={this.getFieldId('y_type')}
                            value={ this.props.params.y_type }
                            onChange={ this._onChange('y_type') }
                        >
                            { this.makeOptions(Scenario_yVals) }
                        </select>
                    </div>

                    <div className={this.getClasses('x_min')}>
                        <label htmlFor={this.getFieldId('x_min')}>Min X-Axis range</label>
                        <input
                            id={this.getFieldId('x_min')}
                            value={ this.props.params.x_min }
                            type="text"
                            placeholder="0"
                            onChange={ this._onChange('x_min') }
                            onBlur={ this.props.handleValidation('x_min', this.handleValidationCallback()) }
                        />
                        {this.renderHelpText(this.props.getValidationMessages('x_min'))}
                    </div>

                    <div className={this.getClasses('x_max')}>
                        <label htmlFor={this.getFieldId('x_max')}>Max X-Axis range</label>
                        <input
                            id={this.getFieldId('x_max')}
                            value={ this.props.params.x_max }
                            type="text"
                            placeholder="100"
                            onChange={ this._onChange('x_max') }
                            onBlur={ this.props.handleValidation('x_max', this.handleValidationCallback()) }
                        />
                        {this.renderHelpText(this.props.getValidationMessages('x_max'))}
                    </div>

                    { /*
                    <div className={this.getClasses('num_points')}>
                        <label htmlFor={this.getFieldId('num_points')}>Number of values</label>
                        <input
                            id={this.getFieldId('num_points')}
                            value={ this.props.params.num_points }
                            type="text"
                            placeholder="10"
                            onChange={ this._onChange('num_points') }
                            onBlur={ this.props.handleValidation('num_points', this.handleValidationCallback()) }
                        />
                        {this.renderHelpText(this.props.getValidationMessages('num_points'))}
                    </div>
                    */ }

                    <div className={this.getClasses('time_offset')}>
                        <label htmlFor={this.getFieldId('time_offset')}>Elapsed time</label>
                        <input
                            id={this.getFieldId('time_offset')}
                            value={ this.props.params.time_offset }
                            type="text"
                            placeholder="0"
                            onChange={ this._onChange('time_offset') }
                            onBlur={ this.props.handleValidation('time_offset', this.handleValidationCallback()) }
                        />
                        {this.renderHelpText(this.props.getValidationMessages('time_offset'))}
                    </div>

                    <input
                        type="submit"
                        value="Update Scenario"
                        disabled={ !calcEnabled }
                    />

                </form>

            </div>
        );
    }

}

ScenarioConfig.PropTypes = {
    id: React.PropTypes.number.isRequired,
    label: React.PropTypes.number.isRequired,
    params: React.PropTypes.object.isRequired,
    logValidationState: React.PropTypes.func.isRequired,
    errors: React.PropTypes.object,
    validate: React.PropTypes.func,
    isValid: React.PropTypes.func,
    handleValidation: React.PropTypes.func,
    getValidationMessages: React.PropTypes.func,
    clearValidations: React.PropTypes.func,
};


export default validation(ValidationStrategy)(ScenarioConfig);
