import React from 'react/lib/ReactIsomorphic.js';
import Pricer from './Pricer.react';
import ResultsStrategy from './ResultsStrategy.react';
import { ResultsStore } from '../stores/ResultsStore.js';
import MessageStore from '../stores/MessageStore';
import Scenarios from './Scenarios.react';
import LegStoreActions from '../actions/LegActions';
import { STRATEGY_JSON_FILE } from '../constants/LegConstants';
import request from 'superagent';
import StrategyList from './StrategyList.react';
import StrategyListStore from '../stores/StrategyListStore';
import classnames from 'classnames';
import StrategyListStoreActions from '../actions/StrategyListStoreActions';


function getItemState() {
    return {
        pricerResults: ResultsStore.getResults(),
        numMessages: MessageStore.getNumMessages(),
        ResultsLegByLeg: ResultsStore.getResultsLegByLeg(),
        strategyDisplayMode: StrategyListStore.getDisplayToggle(),
    };
}


class OptionApp extends React.Component {

    constructor(props) {
        super(props);
        this.state = getItemState();

        this._onChange = this._onChange.bind(this);
        this.getResults = this.getResults.bind(this);
        this.getResultsLegByLeg = this.getResultsLegByLeg.bind(this);
        this.displayResultsByLeg = this.displayResultsByLeg.bind(this);
        this.getNumResultsByLeg = this.getNumResultsByLeg.bind(this);
        this.getNumMessages = this.getNumMessages.bind(this);
        this.getStrategyClasses = this.getStrategyClasses.bind(this);
        this.toggleStrategyMenu = this.toggleStrategyMenu.bind(this);
    }

    componentDidMount() {
        if (process.env.NODE_ENV !== 'production') {
            console.log('Component Mounting!');
        }
        ResultsStore.addChangeListener(this._onChange);
        ResultsStore.addInitListener(this._onChange);
        ResultsStore.addUpdateListener(this._onChange);
        MessageStore.addChangeListener(this._onChange);
        StrategyListStore.addChangeListener(this._onChange);

        // Read JSON file and initialise the legs
        request
            .get(STRATEGY_JSON_FILE)
            .set('Access-Control-Allow-Origin', '*')
            .end((err, res) => {
                if (err) {
                    // handle error
                    if (process.env.NODE_ENV !== 'production') {
                        console.log('JSON read: Error!');
                    }
                }
                else {
                    // handle results
                    if (process.env.NODE_ENV !== 'production') {
                        console.log(res.body);
                    }
                    LegStoreActions.initialise(res.body);
                }
            }); // end of AJAX request
    }

    componentWillUnmount() {
        ResultsStore.removeChangeListener(this._onChange);
        ResultsStore.removeInitListener(this._onChange);
        ResultsStore.removeUpdateListener(this._onChange);
        MessageStore.removeChangeListener(this._onChange);
        StrategyListStore.removeChangeListener(this._onChange);
    }

    getResults() {
        return ResultsStore.getResults();
    }

    getResultsLegByLeg() {
        return ResultsStore.getResultsLegByLeg();
    }

    displayResultsByLeg() {
        return ResultsStore.getResultsByLeg();
    }

    getResultsRefreshStatus() {
        return ResultsStore.needResultRefresh();
    }

    getNumResultsByLeg() {
        return ResultsStore.getNumResultsByLeg();
    }

    getNumMessages() {
        return MessageStore.getNumMessages();
    }

    getStrategyClasses() {
        return classnames({
            'toggle-display': this.state.strategyDisplayMode,
        });
    }

    toggleStrategyMenu() {
        StrategyListStoreActions.toggleDisplay();
    }

    _onChange() {
        this.setState(getItemState());

        if (process.env.NODE_ENV !== 'production') {
            console.log('%%%%% BONG %%%%%%%');
            console.log(JSON.stringify(this.state));
        }
    }

    render() {
        if (process.env.NODE_ENV !== 'production') {
            const ResultsLegByLeg = this.getResultsLegByLeg();
            console.log(ResultsLegByLeg);
        }


        return (

            <div>
                <section id="strategy-bar" className={ this.getStrategyClasses() }>
                    <StrategyList />
                </section>

                <div id="pricer-wrapper">
                    <section id="pricer">
                        <Pricer />
                        <ResultsStrategy
                            results={ this.state.pricerResults }
                        />
                    </section>

                    <section id="scenarios">
                        <Scenarios />
                    </section>
                </div>

                <div id="mask"
                     className={ this.getStrategyClasses() }
                     onClick={ this.toggleStrategyMenu }
                ></div>

            </div>

        );
    }

}


export default OptionApp;
