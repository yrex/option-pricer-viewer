import AppDispatcher from '../dispatcher/AppDispatcher';
import { ScenarioConfigConstants } from '../constants/ScenarioConfigConstants';


const ScenarioConfigStoreActions = {
    add: function add() {
        AppDispatcher.dispatch({
            eventName: ScenarioConfigConstants.ADD_SCENARIO,
        });
    },

    update: function update(id, fieldName, value) {
        AppDispatcher.dispatch({
            eventName: ScenarioConfigConstants.UPDATE_SCENARIO,
            scenarioID: id,
            fieldName: fieldName,
            newValue: value,
        });
    },

    remove: function remove(id) {
        AppDispatcher.dispatch({
            eventName: ScenarioConfigConstants.REMOVE_SCENARIO,
            scenarioID: id,
        });
    },

    updateValidation: function updateValidation(id, status) {
        AppDispatcher.dispatch({
            eventName: ScenarioConfigConstants.SET_SCENARIO_VALIDATION,
            scenarioID: id,
            status: status,
        });
    },
};


export default ScenarioConfigStoreActions;
