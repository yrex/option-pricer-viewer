import AppDispatcher from '../dispatcher/AppDispatcher';
import GreeksCheckBoxConstants from '../constants/GreeksCheckBoxConstants';


const GreeksCheckBoxStoreActions = {

    toggleChecked: function toggleChecked(idx) {
        AppDispatcher.dispatch({
            eventName: GreeksCheckBoxConstants.TOGGLE_CHECKED,
            id: idx,
        });
    },
};


export default GreeksCheckBoxStoreActions;
