import AppDispatcher from '../dispatcher/AppDispatcher';
import StrategyListConstants from '../constants/StrategyListConstants';


const StrategyListStoreActions = {

    toggleDisplay: function toggleChecked() {
        AppDispatcher.dispatch({
            eventName: StrategyListConstants.TOGGLE_DISPLAY,
        });
    },
};


export default StrategyListStoreActions;
