import AppDispatcher from '../dispatcher/AppDispatcher';
import { MessageTypes } from '../constants/MessageConstants';
import { MessageEvents } from '../constants/MessageConstants';


const MessageStoreActions = {

    DeleteMessage: function DeleteMessage(value) {
        AppDispatcher.dispatch({
            eventName: MessageEvents.DELETE_MESSAGE,
            message_id: value,
        });
    },

    AddMessageAndClearResults: function AddMessageAndClearResults(text, textType, label) {
        AppDispatcher.dispatch({
            eventName: MessageEvents.ADD_MESSAGE_CLEAR_RESULTS,
            message: text,
            message_label: { name: label },
            type: textType,
            message_types: [MessageTypes.ERROR,
                            MessageTypes.WARNING],
        });
    },

    // AddMessage: function AddMessage(text, textType) {
    //     AppDispatcher.dispatch({
    //         eventName: MessageEvents.ADD_MESSAGE,
    //         message: text,
    //         type: textType,
    //     });
    // },
};


export default MessageStoreActions;
