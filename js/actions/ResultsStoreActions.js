import AppDispatcher from '../dispatcher/AppDispatcher';
import { ResultsConstants } from '../constants/ResultsConstants.js';
import { MessageTypes } from '../constants/MessageConstants';
import { MessageEvents } from '../constants/MessageConstants';
import { MessageLabels } from '../constants/MessageConstants';
import { CallPut, BuySell, ApiDetails} from '../constants/APIConstants';
import request from 'superagent';
import LegStore from '../stores/LegStore';
import GreeksCheckBoxStore from '../stores/GreeksCheckBoxStore';
import ScenarioConfigStore from '../stores/ScenarioConfigStore';
import { ScenarioGraphActionCreator } from './ScenarioGraphStoreActions';
import Promise from 'bluebird';
import { InvalidScenarioError } from '../common/utils';


const ResultsStoreActions = {

    clear: function clear() {
        AppDispatcher.dispatch({
            eventName: ResultsConstants.CLEAR_RESULTS,
        });
    },

    toggleLegByLeg: function toggleLegByLeg() {
        AppDispatcher.dispatch({
            eventName: ResultsConstants.TOGGLE_LEGBYLEG,
        });
    },

};


// Action Creators
const ResultsActionCreator = {

    // make AJAX call to pricer.
    // on success, display results in the Results box.
    // on failure, display error messages.

    constructArgs: function constructArgs() {
        // iterate through legs to get details

        var legs = LegStore.getAllLegs();
        var price = [];
        var strike = [];
        var dividend = [];
        var expiry = [];
        var sigma = [];
        var interestRate = [];
        var callPut = [];
        var buySell = [];

        // vol and rate are to be given in decimal format.

        legs.forEach((leg) => {
            // price
            price.push(leg.price);
            // strike
            strike.push(leg.strike);
            // dividend
            dividend.push(leg.dividend);
            // time_to_expiry
            expiry.push(leg.time);
            // sigma
            sigma.push(parseFloat(leg.vol) / 100);
            // interestRate
            interestRate.push(parseFloat(leg.rate) / 100);
            // callPut
            callPut.push(
                leg.callput === 'call' ? CallPut.call : CallPut.put
            );
            // buySell
            buySell.push(
                leg.buysell === 'buy' ? BuySell.buy : BuySell.sell
            );
        });

        // ReturnTypes:
        var ReturnTypes = GreeksCheckBoxStore.getCheckedItems();

        return {
            price,
            strike,
            dividend,
            expiry,
            sigma,
            interestRate,
            callPut,
            buySell,
            ReturnTypes,
        };
    },

    /**
     * Send a GET request to the server using the API construct and process the response.
     * The method will handle both success and error responses.
     *
     * @param {boolean} [clearPreviousMsgs = true] - Flag to clear previous messages.
     * @param {boolean} [suppressEmit = false] - Don't send calls to listeners. Use for bulk updates.
     */
    APIGet: function APIGet(clearPreviousMsgs, suppressEmit) {
        var apiURL = ApiDetails.multileg_pricer_url;

        // Check optional parameters and set default values
        var clearMsgs;
        var noEmit;
        typeof clearPreviousMsgs === 'undefined' ? clearMsgs = true : clearMsgs = clearPreviousMsgs;
        typeof suppressEmit === 'undefined' ? noEmit = false : noEmit = suppressEmit;


        // args:
        var args = this.constructArgs();

        //    API Parameters
        //        :parameter list price: the underlying asset price
        //        :parameter list strike: the strike price
        //        :parameter list dividend: dividend yield, given in decimals
        //        :parameter list time_to_expiry: the option time to expiry, given in years
        //        :parameter list sigma: volatility of the underlying asset, given in decimals
        //        :parameter list int_rate: interest rate, given in decimals
        //        :parameter list buy_sell: option buy or sell. given as 'b' or 's'. Uppercase form is also valid.
        //        :parameter list call_put: option call or put. given as 'c' or 'p'. Uppercase form is also valid.
        //        :parameter list ret_types: a comma separated string with the calculations to return

        var ThisObj = this;
        request
            .get(apiURL)
            .query({ price: args.price.join(',') })
            .query({ strike: args.strike.join(',') })
            .query({ dividend: args.dividend.join(',') })
            .query({ time_to_expiry: args.expiry.join(',') })
            .query({ sigma: args.sigma.join(',') })
            .query({ int_rate: args.interestRate.join(',') })
            .query({ call_put: args.callPut.join(',') })
            .query({ buy_sell: args.buySell.join(',') })
            .query({ ret_types: args.ReturnTypes.join(',') })
            .set('Access-Control-Allow-Origin', '*')
            .end((err, res) => {
                if (err) {
                    // handle error
                    // console.log(err);

                    var msg = 'Pricer: ';
                    // if results were returned by API
                    if (typeof res !== 'undefined') {
                        // console.log(res.body.message);
                        msg += res.body.message;
                    }
                    else {
                        msg += 'Oops... Something went wrong!';
                    }

                    ThisObj.handleCalcError(msg, clearMsgs, noEmit);
                }
                else {
                    // handle results
                    if (process.env.NODE_ENV !== 'production') {
                        console.log(res.body);
                    }
                    ThisObj.handleCalcSuccess(res.body, clearMsgs, noEmit);
                }
            }); // end of AJAX request
    },

    /**
     * Send a GET request to the server using the API construct to:
     * 1) Calculate any available scenarios,
     * 2) Calculate pricer results and process the response.
     *
     * The method will handle both success and error responses.
     */
    APIGetWithScenarios: function APIGetWithScenarios() {
        // First, clear all previous messages and don't send a call to listeners
        this.handleMessageCleanup([MessageTypes.WARNING, MessageTypes.ERROR], true);

        // then, update the scenarios
        // Get the scenarios: only IDs are required
        const scenarios = ScenarioConfigStore.getAllScenarios();
        const totalNumScen = scenarios.filter((val) => (val !== undefined)).length;
        if (process.env.NODE_ENV !== 'production') {
            console.log(totalNumScen);
        }


        const clearMsgs = false;
        const noEmit = true;

        // Use a promise to make sure all requests have been resolved before we continue
        // The promises are stored in an array and will keep their original order
        Promise.all(
            scenarios.map((val, idx) => {
                if (process.env.NODE_ENV !== 'production') {
                    console.log('********  SCENARIO index ', idx, ' **********');
                }
                // get the request as a promise and use reflect() so that the promise is always successful.
                // the returned response will then processed.
                return ScenarioGraphActionCreator.APIGetPromise(idx).reflect();
            }
        )).then((results) => {
            // process the results returned from the API for all scenarios

            // Keep a count of the scenario number (not index)

            var scenNum = 0;
            results.forEach((item, itemIdx) => {
                // check that item is not undefined (undefined values returned by promise)

                if (typeof item !== 'undefined') {
                    scenNum += 1;
                    // if the promise was fulfilled
                    if (item.isFulfilled()) {
                        if (process.env.NODE_ENV !== 'production') {
                            console.log(item.value().body);
                        }
                        ScenarioGraphActionCreator.handleCalcSuccess(itemIdx, item.value().body, clearMsgs, noEmit, scenNum);
                    }
                    else {
                        // otherwise process the various errors
                        var msg = 'Profile ' + scenNum + ': ';
                        var msgType = MessageTypes.ERROR;
                        // Check if the request was for an invalid scenario
                        if (item.reason() instanceof InvalidScenarioError) {
                            msg += item.reason().message;
                            msgType = MessageTypes.ERROR;
                        }
                        else if (typeof item.reason().response !== 'undefined') {
                            // else if the API returned a response
                            msg += item.reason().response.body.message;
                        }
                        else {
                            // otherwise return a generic error
                            msg += 'Oops... Something went wrong!';
                        }
                        if (process.env.NODE_ENV !== 'production') {
                            console.log('Promise Err, ', msg);
                        }
                        ScenarioGraphActionCreator.handleCalcError(itemIdx, msg, msgType, clearMsgs, noEmit);
                    }
                }
            });
        }).then(() => {
            // once all scenarios have been processed, request data from for the pricer and update the viewer
            this.APIGet(false);
        });
    },

    /**
     * Method for handling a success response from the server.
     * @param {object} results - The results object is passed to the store for processing.
     * @param {boolean} clearMessages - Clear previous messages
     * @param {boolean} noEmit - Send a call to all listeners. Set to False when performing bulk updates.
     */
    handleCalcSuccess: function handleCalcSuccess(results, clearMessages, noEmit) {
        AppDispatcher.dispatch({
            eventName: ResultsConstants.UPDATE_RESULTS,
            newResults: results,
            message_label: { name: MessageLabels.PRICER },
            message_types: [MessageTypes.WARNING, MessageTypes.ERROR],
            clear_previous_messages: clearMessages,
            suppress_emit: noEmit,
        });
    },

    /**
     * Method for handling an error response from the server. A message is returned to the user.
     * @param {string} text - The message to display.
     * @param {boolean} clearMessages - Clear previous messages
     * @param {boolean} noEmit - Send a call to all listeners. Set to False when performing bulk updates.
     */
    handleCalcError: function handleCalcError(text, clearMessages, noEmit) {
        AppDispatcher.dispatch({
            eventName: MessageEvents.ADD_MESSAGE_CLEAR_RESULTS,
            message: text,
            message_label: { name: MessageLabels.PRICER },
            type: MessageTypes.ERROR,
            message_types: [MessageTypes.ERROR, MessageTypes.WARNING],
            clear_previous_messages: clearMessages,
            suppress_emit: noEmit,
        });
    },

    /**
     * Method for clearing all error messages. Usually used at the start of a new call to the API.
     * @param {MessageTypes|MessageTypes[]} msgType - The type of message being sent. One of MessageTypes.
     * @param {boolean} noEmit - Send a call to all listeners. Set to False when performing bulk updates.
     */
    handleMessageCleanup: function handleMessageCleanup(msgType, noEmit) {
        if (process.env.NODE_ENV !== 'production') {
            console.log('cleaning up!');
        }
        AppDispatcher.dispatch({
            eventName: MessageEvents.DELETE__MESSAGES_BY_TYPE,
            message_types: msgType,
            suppress_emit: noEmit,
        });
    },
};




module.exports = {
    ResultsStoreActions,
    ResultsActionCreator,
};
