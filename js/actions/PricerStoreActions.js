import AppDispatcher from '../dispatcher/AppDispatcher';
import PricerConstants from '../constants/PricerConstants';


const PricerStoreActions = {

    setValidation: function setValidation(status) {
        AppDispatcher.dispatch({
            eventName: PricerConstants.SET_VALID,
            status: status,
        });
    },
};


export default PricerStoreActions;
