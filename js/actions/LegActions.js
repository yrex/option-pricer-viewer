import AppDispatcher from '../dispatcher/AppDispatcher';
import { LegConstants } from '../constants/LegConstants';


const LegStoreActions = {
    add: function add() {
        AppDispatcher.dispatch({
            eventName: LegConstants.ADD_LEG,
        });
    },

    update: function update(id, fieldName, value) {
        AppDispatcher.dispatch({
            eventName: LegConstants.UPDATE_LEG,
            legID: id,
            fieldName: fieldName,
            newValue: value,
        });
    },

    remove: function remove(id) {
        AppDispatcher.dispatch({
            eventName: LegConstants.REMOVE_LEG,
            legID: id,
        });
    },

    initialise: function initialise(legTypes) {
        AppDispatcher.dispatch({
            eventName: LegConstants.INIT,
            legTypes: legTypes,
        });
    },

    setLegPreset: function setLegPreset(value) {
        AppDispatcher.dispatch({
            eventName: LegConstants.SET_LEG_PRESET,
            presetName: value,
        });
    }
};


export default LegStoreActions;
