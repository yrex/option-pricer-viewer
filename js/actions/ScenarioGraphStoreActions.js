import AppDispatcher from '../dispatcher/AppDispatcher';
import { ResultsConstants } from '../constants/ResultsConstants.js';
import { MessageTypes } from '../constants/MessageConstants';
import { MessageLabels } from '../constants/MessageConstants';
import { CallPut, BuySell, ApiDetails } from '../constants/APIConstants';
import { BSParams } from '../constants/APIConstants';
import LegStore from '../stores/LegStore';
import ScenarioConfigStore from '../stores/ScenarioConfigStore';
import ScenarioGraphConstants from '../constants/ScenarioGraphConstants.js';
import assign from 'object-assign';
import Promise from 'bluebird';
import { InvalidScenarioError } from '../common/utils';


// SuperAgent with a promise wrapper
const agent = require('superagent-promise')(require('superagent'), Promise);

const ScenarioGraphStoreActions = {

    clear: function clear() {
        AppDispatcher.dispatch({
            eventName: ResultsConstants.CLEAR_RESULTS,
        });
    },

    toggleLegByLeg: function toggleLegByLeg() {
        AppDispatcher.dispatch({
            eventName: ResultsConstants.TOGGLE_LEGBYLEG,
        });
    },

};


// Action Creators
const ScenarioGraphActionCreator = {

    // make AJAX call to scenario calculator.
    // on success, display results as a scenario graph.
    // on failure, display error messages.

    constructPricerArgs: function constructPricerArgs() {
        // iterate through legs to get details

        var legs = LegStore.getAllLegs();
        var price = [];
        var strike = [];
        var dividend = [];
        var expiry = [];
        var sigma = [];
        var interestRate = [];
        var callPut = [];
        var buySell = [];

        // vol and rate are to be given in decimal format.

        legs.forEach((leg) => {
            // price
            price.push(leg.price);
            // strike
            strike.push(leg.strike);
            // dividend
            dividend.push(leg.dividend);
            // time_to_expiry
            expiry.push(leg.time);
            // sigma
            sigma.push(parseFloat(leg.vol) / 100);
            // interestRate
            interestRate.push(parseFloat(leg.rate) / 100);
            // callPut
            callPut.push(
                leg.callput === 'call' ? CallPut.call : CallPut.put
            );
            // buySell
            buySell.push(
                leg.buysell === 'buy' ? BuySell.buy : BuySell.sell
            );
        });

        return {
            price,
            strike,
            dividend,
            expiry,
            sigma,
            interestRate,
            callPut,
            buySell,
        };
    },

    constructScenarioArgs: function constructScenariosArgs(scenarioID) {
        var scenario = ScenarioConfigStore.getScenario(scenarioID);
        var outputScenario = assign({}, scenario);

        if (typeof outputScenario !== 'undefined') {
            // if the x_type member is a vol or rate, then scale x_min and x_max by 100.
            if (outputScenario.x_type === BSParams.SIGMA || outputScenario.x_type === BSParams.R) {
                outputScenario.x_min = outputScenario.x_min / 100;
                outputScenario.x_max = outputScenario.x_max / 100;
            }

            return outputScenario;
        }
        // otherwise return undefined
        return undefined;
    },

    /**
     * Send a GET request to the server using the API and process the response.
     * The method will handle both success and error responses.
     * @param {number} scenarioID - The ID of the scenario being requested.
     * @param {number} scenarioLabel - The label of the scenario being requested.
     *                 This is passed to messages to identify them.
     * @param {boolean} [clearPreviousMsgs = true] - Flag to clear previous messages.
     * @param {boolean} [suppressEmit = false] - Don't send calls to listeners. Use for bulk updates.
     */
    APIGet: function APIGet(scenarioID, scenarioLabel, clearPreviousMsgs, suppressEmit) {
        // Check optional parameters and set default values
        var clearMsgs;
        var noEmit;
        typeof clearPreviousMsgs === 'undefined' ? clearMsgs = true : clearMsgs = clearPreviousMsgs;
        typeof suppressEmit === 'undefined' ? noEmit = false : noEmit = suppressEmit;

        if (process.env.NODE_ENV !== 'production') {
            console.log('clear msg:', clearMsgs, 'Don\'t emit:', noEmit);
        }

        return this.APIGetPromise(scenarioID, scenarioLabel)
            .then((response) => {
                // if the promise was fulfilled
                this.handleCalcSuccess(scenarioID, response.body, clearMsgs, noEmit, scenarioLabel);
            }, (error) => {
                // otherwise process the various errors
                var msg = 'Profile ' + scenarioLabel + ': ';
                // Check if the request was for an invalid scenario
                if (error instanceof InvalidScenarioError) {
                    msg += error.message;
                }
                else if (typeof error.response !== 'undefined') {
                    // else if the API returned a response
                    msg += error.response.body.message;
                }
                else {
                    // otherwise return a generic error
                    msg += 'Oops... Something went wrong!';
                }

                if (process.env.NODE_ENV !== 'production') {
                    console.log('Promise Err, ', msg);
                }
                var msgTypeof = MessageTypes.ERROR;
                this.handleCalcError(scenarioID, msg, msgTypeof, clearMsgs, noEmit);
            });
    },

    /**
     * Send a GET request to the server using the API construct and return the results as a promise.
     * The results (responses and errors) have to then be processed
     * @param {number} scenarioID - The ID of the scenario being requested.
     * @returns {Promise} The results from the API are returned as a promise
     */
    APIGetPromise: function APIGetPromise(scenarioID) {
        var apiURL = ApiDetails.scenario_pricer_url;

        // args:
        var pricerArgs = this.constructPricerArgs();
        var scenarioArgs = this.constructScenarioArgs(scenarioID);

        // default message for errors
        var msg = '';

        // Only send the request to the API if the scenario is not disabled. Otherwise show a warning message.
        if (!scenarioArgs.enabled) {
            msg += 'Check inputs';

            return Promise.reject(new InvalidScenarioError(msg));
        }
        //    API Parameters
        //        :parameter list price: the underlying asset price
        //        :parameter list strike: the strike price
        //        :parameter list dividend: dividend yield, given in decimals
        //        :parameter list time_to_expiry: the option time to expiry, given in years
        //        :parameter list sigma: volatility of the underlying asset, given in decimals
        //        :parameter list int_rate: interest rate, given in decimals
        //        :parameter list buy_sell: option buy or sell. given as 'b' or 's'. Uppercase form is also valid.
        //        :parameter list call_put: option call or put. given as 'c' or 'p'. Uppercase form is also valid.
        //        :parameter str y_type: value being calculated, of type ReturnTypes.
        //        :parameter str x_type: value modified in scenario, of type BSParams.
        //        :parameter float xmin: the minimum value in scenario.
        //        :parameter float xmax: the maximum value in scenario.
        //        :parameter int num_points: the number of points in scenario. A maximum number MAX_SCENARIO_POINTS is allowed.
        //        :parameter float time_offset: time epoch to calculate value/greek of Option scenario.
        //        :parameter bool include_original_x_vals: whether to include the original x-axis parameter. Default is False.
        //        :parameter bool add_support_points: whether to include extra support points on either side of the original
        //                   x-axis parameter. The default is False. If True, include_original_x_vals
        //                   must be True as well, otherwise it is ignored.

        return agent.get(apiURL)
            .query({price: pricerArgs.price.join(',')})
            .query({strike: pricerArgs.strike.join(',')})
            .query({dividend: pricerArgs.dividend.join(',')})
            .query({time_to_expiry: pricerArgs.expiry.join(',')})
            .query({sigma: pricerArgs.sigma.join(',')})
            .query({int_rate: pricerArgs.interestRate.join(',')})
            .query({call_put: pricerArgs.callPut.join(',')})
            .query({buy_sell: pricerArgs.buySell.join(',')})
            .query({y_type: scenarioArgs.y_type})
            .query({x_type: scenarioArgs.x_type})
            .query({xmin: scenarioArgs.x_min})
            .query({xmax: scenarioArgs.x_max})
            .query({num_points: scenarioArgs.num_points})
            .query({time_offset: scenarioArgs.time_offset})
            .query({include_original_x_vals: scenarioArgs.inc_orig_x_vals})
            .query({add_support_points: scenarioArgs.add_support_points})
            .set('Access-Control-Allow-Origin', '*')
            .end();
    },

    /**
     * Method for handling a success response from the server.
     * @param {number} graphID - The ID for the graph to send data to
     * @param {object} results - The results object is passed to the store for processing.
     * @param {boolean} clearMessages - Clear previous messages
     * @param {boolean} noEmit - Send a call to all listeners. Set to False when performing bulk updates.
     * @param {number} scenarioNumber - The identifier for the scenario. Used for prefix in API error messages.
     */
    handleCalcSuccess: function handleCalcSuccess(graphID, results, clearMessages, noEmit, scenarioNumber) {
        if (process.env.NODE_ENV !== 'production') {
            console.log('success!');
        }
        AppDispatcher.dispatch({
            eventName: ScenarioGraphConstants.UPDATE_GRAPH,
            graphID: graphID,
            newResults: results,
            message_label: { name: MessageLabels.SCENARIO, id: graphID },
            message_types: [MessageTypes.WARNING, MessageTypes.ERROR],
            clear_previous_messages: clearMessages,
            suppress_emit: noEmit,
            msg_prefix: scenarioNumber,
        });
    },

    /**
     * Method for handling an error response from the server. A message is returned to the user.
     * @param {number} graphID - The ID for the graph to send data to
     * @param {string} text - The message to display.
     * @param {MessageTypes} msgType - The type of message being sent. One of MessageTypes.
     * @param {boolean} clearMessages - Clear previous messages
     * @param {boolean} noEmit - Send a call to all listeners. Set to False when performing bulk updates.
     */
    handleCalcError: function handleCalcError(graphID, text, msgType, clearMessages, noEmit) {
        if (process.env.NODE_ENV !== 'production') {
            console.log('fail');
        }
        AppDispatcher.dispatch({
            eventName: ScenarioGraphConstants.CLEAR_GRAPH_ADD_MESSAGE,
            message: text,
            message_label: { name: MessageLabels.SCENARIO, id: graphID },
            type: msgType,
            graphID: graphID,
            message_types: [MessageTypes.ERROR, MessageTypes.WARNING],
            clear_previous_messages: clearMessages,
            suppress_emit: noEmit,
        });
    },
};


module.exports = {
    ScenarioGraphStoreActions,
    ScenarioGraphActionCreator,
};
