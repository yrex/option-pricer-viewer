import { EventEmitter } from 'events';
import assign from 'object-assign';
import { CHANGE_EVENT, INIT_EVENT, UPDATE_EVENT } from '../constants/onEventConstants';
import AppDispatcher from '../dispatcher/AppDispatcher';
import { ResultsConstants } from '../constants/ResultsConstants.js';
import { MessageEvents } from '../constants/MessageConstants';
import { isUndefined } from '../common/utils';
import { LegConstants, DEFAULT_LEG_TYPE } from '../constants/LegConstants';


const ResultsStore = assign({}, EventEmitter.prototype, {

    defaultResults: {
        value: 0,
        delta: 0,
        gamma: 0,
        vega: 0,
        theta: 0,
        rho: 0,
    },

    defaultLegByLegResults: {
        value: [0],
        delta: [0],
        gamma: [0],
        vega: [0],
        theta: [0],
        rho: [0],
    },

    strategyNumResults: {},
    results: {},
    results_LegByLeg: {},
    numResults: 0,
    DisplayResultsByLeg: true,
    isResultStale: false,

    /**
     * Returns the number of results-by-leg
     * @returns {number} The number of results-by-leg
     */
    getNumResultsByLeg: function getNumResultsByLeg() {
        return this.numResults;
    },

    /**
     * Return the results object. If the object hasn't been populated, the default value is returned.
     * @returns {*}
     */
    getResults: function getResults() {
        if (Object.getOwnPropertyNames(this.results).length === 0) {
            return this.defaultResults;
        }

        // else...
        return this.results;
    },

    /**
     * Return the results_LegByLeg object. If the object hasn't been populated, the
     * default value is returned.
     * @returns {*}
     */
    getResultsLegByLeg: function getResultsLegByLeg() {
        if (Object.getOwnPropertyNames(this.results_LegByLeg).length === 0) {
            return this.defaultLegByLegResults;
        }

        // else...
        return this.results_LegByLeg;
    },

    /**
     * Return the leg by leg results in the following format:
     *    [{value: x, delta: x, gamma: x, etc.}, {value: x, delta: x, gamma: x, etc.}, etc]
     * i.e., as an array of objects, where each array item is a leg and contains the information relating
     * to that leg.
     * Note: The function retains the structure of the items in the original source, that is, any undefined values
     * will be transfered to the formatted output.
     * @returns {Array}
     */
    getResultsLegByLegFormatted: function getResultsLegByLegFormatted() {
        var numResults = this.getNumResultsByLeg();
        var output = new Array(this.getNumResultsByLeg());
        var originalInput;

        if (numResults === 0) {
            originalInput = this.defaultLegByLegResults;
        }
        else {
            originalInput = this.results_LegByLeg;
        }

        // get the index of item locations in array
        // as some array locations may be undefined.
        var idxs = [];
        originalInput.value.forEach((item, idx) => {
            idxs.push(idx);
        });

        Object.keys(originalInput).forEach((keyName, keyIdx) => {
            originalInput[keyName].forEach((item, itemIdx) => {
                if (isUndefined(output[itemIdx])) {
                    output[itemIdx] = { [keyName]: item };
                }
                else {
                    output[itemIdx][keyName] = item;
                }
            });
        });

        if (process.env.NODE_ENV !== 'production') {
            console.log('*********');
            console.log(JSON.stringify(originalInput));
            console.log(JSON.stringify(output));
            console.log('*********');
        }

        return output;
    },

    /**
     * Flag that determintes whether the results should be displayed as an aggregate, or
     * as separate results for each leg.
     * @returns {boolean} Returns true if results should be displayed leg by leg.
     */
    getResultsByLeg: function getResultsByLeg() {
        return this.DisplayResultsByLeg;
    },

    /**
     * Update the flag for displaying results as an aggregate or leg by leg.
     */
    toggleLegByLeg: function toggleLegByLeg() {
        this.DisplayResultsByLeg = !this.DisplayResultsByLeg;
    },

    /**
     * Set the flag to indicate the current results are stale and a recalculation is required.
     */
    RequireUpdate: function RequireUpdate() {
        this.isResultStale = true;
    },

    /**
     * Return a boolean to indicate if the results need to be recalculated.
     * @returns {boolean} Returns true if results should be recalculated.
     */
    needResultRefresh: function needResultRefresh() {
        return this.isResultStale;
    },

    /**
     * Update the aggregate results and the leg by leg results using the input values.
     * @param {object} newResults An object with values to populate the results and results_LegByLeg object.
     */
    update: function update(newResults) {
        this.results.value = isUndefined(newResults.VALUE) ? 0 : newResults.VALUE[0];
        this.results.delta = isUndefined(newResults.DELTA) ? 0 : newResults.DELTA[0];
        this.results.gamma = isUndefined(newResults.GAMMA) ? 0 : newResults.GAMMA[0];
        this.results.vega = isUndefined(newResults.VEGA) ? 0 : newResults.VEGA[0];
        this.results.theta = isUndefined(newResults.THETA) ? 0 : newResults.THETA[0];
        this.results.rho = isUndefined(newResults.RHO) ? 0 : newResults.RHO[0];

        if (process.env.NODE_ENV !== 'production') {
            console.log(newResults);
        }

        this.results_LegByLeg.value = isUndefined(newResults.VALUE)
            ? this.setArrayUsingTemplate(0, this.results_LegByLeg.value)
            : this.setArrayUsingTemplate(newResults.VALUE[1], this.results_LegByLeg.value);
        this.results_LegByLeg.delta = isUndefined(newResults.DELTA)
            ? this.setArrayUsingTemplate(0, this.results_LegByLeg.delta)
            : this.setArrayUsingTemplate(newResults.DELTA[1], this.results_LegByLeg.delta);
        this.results_LegByLeg.gamma = isUndefined(newResults.GAMMA)
            ? this.setArrayUsingTemplate(0, this.results_LegByLeg.gamma)
            : this.setArrayUsingTemplate(newResults.GAMMA[1], this.results_LegByLeg.gamma);
        this.results_LegByLeg.vega = isUndefined(newResults.VEGA)
            ? this.setArrayUsingTemplate(0, this.results_LegByLeg.vega)
            : this.setArrayUsingTemplate(newResults.VEGA[1], this.results_LegByLeg.vega);
        this.results_LegByLeg.theta = isUndefined(newResults.THETA)
            ? this.setArrayUsingTemplate(0, this.results_LegByLeg.theta)
            : this.setArrayUsingTemplate(newResults.THETA[1], this.results_LegByLeg.theta);
        this.results_LegByLeg.rho = isUndefined(newResults.RHO)
            ? this.setArrayUsingTemplate(0, this.results_LegByLeg.rho)
            : this.setArrayUsingTemplate(newResults.RHO[1], this.results_LegByLeg.rho);

        if (process.env.NODE_ENV !== 'production') {
            console.log(JSON.stringify(this.results_LegByLeg));
        }
    },

    /**
     * Clear all values in the results and results_LegByLeg objects
     */
    clear: function clear() {
        this.results = assign({}, this.defaultResults);
        // this.results_LegByLeg = assign({}, this.defaultResults);
        // add the required number of elements in the results_LegByLeg member
        this.results_LegByLeg.value = this.setArrayUsingTemplate(0, this.results_LegByLeg.value);
        this.results_LegByLeg.delta = this.setArrayUsingTemplate(0, this.results_LegByLeg.delta);
        this.results_LegByLeg.gamma = this.setArrayUsingTemplate(0, this.results_LegByLeg.gamma);
        this.results_LegByLeg.vega = this.setArrayUsingTemplate(0, this.results_LegByLeg.vega);
        this.results_LegByLeg.theta = this.setArrayUsingTemplate(0, this.results_LegByLeg.theta);
        this.results_LegByLeg.rho = this.setArrayUsingTemplate(0, this.results_LegByLeg.rho);

        this.isResultStale = false;
    },

    /**
     * Add an element to each of the leg-by-leg result members
     * This is done so the number of results is kept the same as the number of legs.
     */
    addLegByLegResult: function addLegByLegResult() {

        if (process.env.NODE_ENV !== 'production') {
            console.log(this.results_LegByLeg);
        }

        this.results_LegByLeg.value.push(0);
        this.results_LegByLeg.delta.push(0);
        this.results_LegByLeg.gamma.push(0);
        this.results_LegByLeg.vega.push(0);
        this.results_LegByLeg.theta.push(0);
        this.results_LegByLeg.rho.push(0);

        this.numResults += 1;
    },

    /**
     * Remove an element from each of the leg-by-leg result members
     * This is done so the number of results is kept the same as the number of legs.
     * @param {integer} elementIndex - The index of the element to remove
     */
    removeLegByLegResult: function removeLegByLegResults(elementIndex) {

        if (process.env.NODE_ENV !== 'production') {
            console.log(JSON.stringify(this.results_LegByLeg));
            console.log(elementIndex);
        }

        var didDelete = false;

        if (!isUndefined(this.results_LegByLeg.value)) {
            delete this.results_LegByLeg.value[elementIndex];
            didDelete = true;
        }

        if (!isUndefined(this.results_LegByLeg.delta)) {
            delete this.results_LegByLeg.delta[elementIndex];
            didDelete = true;
        }

        if (!isUndefined(this.results_LegByLeg.gamma)) {
            delete this.results_LegByLeg.gamma[elementIndex];
            didDelete = true;
        }

        if (!isUndefined(this.results_LegByLeg.vega)) {
            delete this.results_LegByLeg.vega[elementIndex];
            didDelete = true;
        }

        if (!isUndefined(this.results_LegByLeg.theta)) {
            delete this.results_LegByLeg.theta[elementIndex];
            didDelete = true;
        }

        if (!isUndefined(this.results_LegByLeg.rho)) {
            delete this.results_LegByLeg.rho[elementIndex];
            didDelete = true;
        }

        if (didDelete) {
            this.numResults -= 1;
            this.isResultStale = true;
        }

        if (process.env.NODE_ENV !== 'production') {
            console.log(JSON.stringify(this.results_LegByLeg));
        }
    },

    /**
     * set the number of leg-by-leg results required for various leg combinations (strategies).
     * @param {object} legTypes - the various strategies
     */
    initialise: function initialise(legTypes) {
        Object.keys(legTypes).forEach((strategyName) => {
            this.strategyNumResults[strategyName] = legTypes[strategyName].strategy.length;
        });
    },

    /**
     * Set the default number of leg-by-leg results on application startup.
     * The default leg type is the name given in the strategies.json file.
     */
    setDefaultLegByLegResults: function setDefaultLegs() {
        const legType = DEFAULT_LEG_TYPE || '';
        this.updateResultsFromPreset(legType);
    },

    /**
     * Update the number of results objects required in the results array, to match the number of
     * legs in the chosen preset. If no preset is provided (i.e. empty string), a default of 1 leg is used.
     *
     * @param {string} legType - The leg type, corresponding to the loaded strategies,
     * or an empty string.
     */
    updateResultsFromPreset: function updateResultsFromPreset(legType) {
        // get the number of results required in the strategy.
        // If it cannot be found / not set, use the default number of 1.
        if (legType && this.strategyNumResults[legType]) {
            this.numResults = this.strategyNumResults[legType];
        }
        else {
            this.numResults = 1;
        }

        // reset the total result values
        this.results.value = 0;
        this.results.delta = 0;
        this.results.gamma = 0;
        this.results.vega = 0;
        this.results.theta = 0;
        this.results.rho = 0;

        // add the required number of elements in the results_LegByLeg member
        this.results_LegByLeg.value = this.setDefaultLegByLegValue(this.numResults);
        this.results_LegByLeg.delta = this.setDefaultLegByLegValue(this.numResults);
        this.results_LegByLeg.gamma = this.setDefaultLegByLegValue(this.numResults);
        this.results_LegByLeg.vega = this.setDefaultLegByLegValue(this.numResults);
        this.results_LegByLeg.theta = this.setDefaultLegByLegValue(this.numResults);
        this.results_LegByLeg.rho = this.setDefaultLegByLegValue(this.numResults);

        this.isResultStale = true;
    },

    /**
     * Set the default value of 0 for a given key of the leg-by-leg results this.results_LegByLeg.
     * The value of 0 is used in an array of size numResults.
     * @param {number} numResults -  The number of elements in the array. The default value of 0
     * will be set for each item in the array.
     * @returns {Array} returns an array of zeroes with size numResults.
     */
    setDefaultLegByLegValue: function setDefaultLegByLegValue(numResults) {
        return new Array(numResults).fill(0);
    },

    /**
     Populate an array with 0 values. If a template array is given, the output structure will
     * match the template, i.e. any undefined elements will be present in the output as well.
     *
     * If inputValue is neither a number or array, then a default value of 0 will be used to
     * populate the array.
     *
     * @param {number | Array} inputValue - A number or Array that is the source of the data.
     * @param {Array} templateArray - A template Array that will be used to distribute and
     * align the inputValue.
     */
    setArrayUsingTemplate: function setArrayUsingTemplate(inputValue, templateArray) {
        var output = [];
        var inputIdx = 0;

        // first create an array with the same size as the templateArray.
        // Note that if the last element of templateArray is undefined, it won't be included
        // in loops. By creating the empty array as a first step, ensures the structure of the two arrays
        // is the same.
        output = new Array(templateArray.length);

        // if input is an array
        if (Array.isArray(inputValue)) {

            templateArray.forEach((d, idx) => {
                output[idx] = inputValue[inputIdx];
                inputIdx += 1;
            });
        }
        else {
            // if input is a number
            if (typeof inputValue === 'number') {
                templateArray.forEach((d, idx) => {
                    output[idx] = inputValue;
                });
            }
            // if not an array or number, then use a default value of 0.
            else {
                templateArray.forEach((d, idx) => {
                    output[idx] = 0;
                });
            }
        }
        return output;
    },

    emitChange: function emitChange() {
        this.emit(CHANGE_EVENT);
    },

    emitUpdateRequired: function emitChange() {
        this.emit(UPDATE_EVENT);
    },

    emitInit: function emitInit() {
        this.emit(INIT_EVENT);
    },

    addChangeListener: function addChangeListener(callback) {
        if (process.env.NODE_ENV !== 'production') {
            console.log('%c ResultStore Change Listener ', 'background: #666; color: #bada55');
        }
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    },

    addInitListener: function addInitListener(callback) {
        if (process.env.NODE_ENV !== 'production') {
            console.log('%c ResultStore Init Listener ', 'background: #666; color: #bada55');
        }
        this.on(INIT_EVENT, callback);
    },

    removeInitListener: function removeInitListener(callback) {
        this.removeListener(INIT_EVENT, callback);
    },

    addUpdateListener: function addInitListener(callback) {
        if (process.env.NODE_ENV !== 'production') {
            console.log('%c ResultStore Init Listener ', 'background: #666; color: #bada55');
        }
        this.on(UPDATE_EVENT, callback);
    },

    removeUpdateListener: function removeInitListener(callback) {
        this.removeListener(UPDATE_EVENT, callback);
    },
});


// Register callbacks to ResultsStore
const dispatchToken_ClearResults = AppDispatcher.register((payload) => {
    switch (payload.eventName) {

        // LegStore callbacks
        case ResultsConstants.UPDATE_RESULTS:

            // clear old results before updating the results
            //AppDispatcher.waitFor([dispatchToken_ClearResults]);
            ResultsStore.clear();
            ResultsStore.update(payload.newResults);
            // don't emit changes
            //ResultsStore.emitChange();
            break;

        case ResultsConstants.CLEAR_RESULTS:
            ResultsStore.clear();
            // don't emit changes
            break;

        case ResultsConstants.TOGGLE_LEGBYLEG:
            ResultsStore.toggleLegByLeg();
            ResultsStore.emitChange();
            break;

        case MessageEvents.ADD_MESSAGE_CLEAR_RESULTS:
            ResultsStore.clear();
            // don't emit changes
            //ResultsStore.emitChange();
            break;

        case LegConstants.INIT:
            ResultsStore.initialise(payload.legTypes);
            ResultsStore.setDefaultLegByLegResults();
            if (process.env.NODE_ENV !== 'production') {
                console.log(ResultsStore.results_LegByLeg);
            }
            ResultsStore.emitInit();
            break;

        case LegConstants.ADD_LEG:
            ResultsStore.addLegByLegResult();
            ResultsStore.emitChange();
            break;

        case LegConstants.REMOVE_LEG:
            ResultsStore.removeLegByLegResult(payload.legID);
            ResultsStore.emitChange();
            break;

        case LegConstants.UPDATE_LEG:
            // A leg update does not require the result store values to be updated.
            // However, the results will be stale. A special event is used for this case.
            // This is to avoid re-rendering information from the ResultsStore.
            ResultsStore.RequireUpdate();
            ResultsStore.emitUpdateRequired();
            break;

        case LegConstants.SET_LEG_PRESET:
            ResultsStore.updateResultsFromPreset(payload.presetName);
            ResultsStore.emitChange();
            break;

        default:
            // no operation
    }
});


module.exports = {
    ResultsStore,
    dispatchToken_ClearResults,
};
