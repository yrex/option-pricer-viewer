import { EventEmitter } from 'events';
import assign from 'object-assign';
import CHANGE_EVENT from '../constants/onEventConstants';
import AppDispatcher from '../dispatcher/AppDispatcher';
import { Greeks } from '../constants/APIConstants';
import GreeksCheckBoxConstants from '../constants/GreeksCheckBoxConstants';


const GreeksCheckBoxStore = assign({}, EventEmitter.prototype, {


    // checkbox group data
    DefaultGreeksCheckBoxData: [
        { label: 'Value', value: Greeks.value, checked: true  },
        { label: 'Delta', value: Greeks.delta, checked: true },
        { label: 'Gamma', value: Greeks.gamma, checked: true },
        { label: 'Vega', value: Greeks.vega, checked: true },
        { label: 'Theta', value: Greeks.theta, checked: true },
        { label: 'Rho', value: Greeks.rho, checked: true },
    ],

    CheckBoxData: [],

    /**
     * Get the current data for the checkbox group
     * @returns {Array} the checkbox group data
     */
    getData: function getData() {
        // initialise CheckBoxData if it is not set
        if (this.CheckBoxData.length === 0) {
            this.CheckBoxData = JSON.parse(JSON.stringify(this.DefaultGreeksCheckBoxData));
        }

        return this.CheckBoxData;
    },

    /**
     * Toggle the checked member of a particular item in the data array.
     * @param {number} idx The index of an item in the data array
     */
    toggleCheckedValue: function toggleCheckedValue(idx) {
        // Update the checked state of the data
        this.CheckBoxData.forEach((d, key) => {
            if (key === idx) {
                d.checked = !d.checked;
            }
        });

    },

    /**
     * Get the array of items which have a checked member set to true.
     * @returns {Array} The array of data items with a checked member set to true.
     */
    getCheckedItems : function getCheckedItems() {
        var checkedItems = [];

        this.CheckBoxData.forEach((d) => {
            if (d.checked) {
                checkedItems.push(d.value);
            }
        });

        return checkedItems;
    },

    emitChange: function emitChange() {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    },

});


//Register callbacks to GreeksCheckBoxStore
AppDispatcher.register((payload) => {
    switch (payload.eventName) {
        // LegStore callbacks
        case GreeksCheckBoxConstants.TOGGLE_CHECKED:

            GreeksCheckBoxStore.toggleCheckedValue(payload.id);
            GreeksCheckBoxStore.emitChange();
            break;

        default:
        // no operation
    }
});


export default GreeksCheckBoxStore;
