import { EventEmitter } from 'events';
import assign from 'object-assign';
import CHANGE_EVENT from '../constants/onEventConstants';
import AppDispatcher from '../dispatcher/AppDispatcher';
import StrategyListConstants from '../constants/StrategyListConstants';
import { LegConstants } from '../constants/LegConstants';


const StrategyListStore = assign({}, EventEmitter.prototype, {

    displayToggle: false,

    /**
     * Set the display toggle state
     */
    setDisplayToggle: function setDisplayToggle() {
        this.displayToggle = !this.displayToggle;
    },

    /**
     * Return the display toggle flag
     * @returns {boolean}
     */
    getDisplayToggle: function getDisplayToggle() {
        return this.displayToggle;
    },

    emitChange: function emitChange() {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    },

});


//Register callbacks to GreeksCheckBoxStore
AppDispatcher.register((payload) => {
    switch (payload.eventName) {
        // LegStore callbacks
        case StrategyListConstants.TOGGLE_DISPLAY:
            StrategyListStore.setDisplayToggle();
            StrategyListStore.emitChange();
            break;

        case LegConstants.SET_LEG_PRESET:
            StrategyListStore.setDisplayToggle();
            StrategyListStore.emitChange();
            break;

        default:
        // no operation
    }
});

export default StrategyListStore;