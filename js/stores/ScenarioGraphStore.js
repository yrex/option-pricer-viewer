import { EventEmitter } from 'events';
import assign from 'object-assign';
import { ScenarioConfigConstants } from '../constants/ScenarioConfigConstants';
import CHANGE_EVENT from '../constants/onEventConstants';
import AppDispatcher from '../dispatcher/AppDispatcher';
import ScenarioGraphConstants from '../constants/ScenarioGraphConstants.js';
import { isUndefined } from '../common/utils';


const defaultGraph = {
    yValSummary: [],
    yValByLeg: [],
    xVals: [],
    LegByLeg: false,
};

const ScenarioGraphStore = assign({}, EventEmitter.prototype, {

    // graphs: [],
    graphs: [assign({}, defaultGraph)],
    numGraphs: 1,

    /**
     * Return all graphs
     * @returns {Array} array of graph objects
     */
    getAll: function getAll() {
        // return a deep copy, so that it can be used for
        // component update checks
        return JSON.parse(JSON.stringify(this.graphs));
    },

    /**
     * Add a graph to the graph collection. The default graph object definition is used.
     */
    add: function add() {
        const initGraph = assign({}, defaultGraph);

        this.graphs.push(initGraph);
        this.numGraphs += 1;
    },

    /**
     * Clear a graph and set it to the default values
     * @param {number} graphID - The ID of the graph to be cleared
     */
    clear: function clear(graphID) {
        this.graphs[graphID] = assign({}, defaultGraph);
    },

    /**
     * Remove a graph from the graph collection.
     * @param {number} graphID - The ID of the graph to be removed
     */
    remove: function remove(graphID) {
        delete this.graphs[graphID];
        this.numGraphs -= 1;
    },

    /**
     * Update a graph in the graph collection.
     * @param {number} graphID - The ID of the graph to be removed
     * @param {object} newValues - The new values. This is in the format of the scenario pricer API
     */
    updateValuesfromAPI: function updateValuesfromAPI(graphID, newValues) {
        // check if results have been returned. If not, set the graph to use default values.
        if (isUndefined(newValues.SCENARIO_TOTAL)) {
            this.graphs[graphID] = assign({}, defaultGraph);
        }
        else {
            this.graphs[graphID].yValSummary = JSON.parse(JSON.stringify(newValues.SCENARIO_TOTAL));
            this.graphs[graphID].yValByLeg = JSON.parse(JSON.stringify(newValues.SCENARIO_LEGS));
            this.graphs[graphID].xVals = JSON.parse(JSON.stringify(newValues.X_VALUES));
        }
    },

    /**
     * Toggle the leg-by-leg flag for a given graph
     * @param {number} graphID - The ID of the graph to be updated
     */
    toggleLegByLeg: function toggleLegByLeg(graphID) {
        this.graphs[graphID].LegByLeg = !this.graphs[graphID].LegByLeg;
    },

    emitChange: function emitChange() {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    },
});

// Separate dispatch callbacks for tokens
const dispatchToken_GraphUpdate = AppDispatcher.register((payload) => {
    switch (payload.eventName) {

        case ScenarioGraphConstants.UPDATE_GRAPH:
            if (process.env.NODE_ENV !== 'production') {
            console.log('graph update');
            }
            ScenarioGraphStore.updateValuesfromAPI(payload.graphID, payload.newResults);
            //ScenarioGraphStore.emitChange();
            break;

        case ScenarioGraphConstants.CLEAR_GRAPH_ADD_MESSAGE:
            if (process.env.NODE_ENV !== 'production') {
                console.log('graph clear');
            }
            ScenarioGraphStore.clear(payload.graphID);
            // don't emit change event
            break;

        case ScenarioConfigConstants.ADD_SCENARIO:
            ScenarioGraphStore.add();
            // don't emit change event
            break;

        case ScenarioConfigConstants.REMOVE_SCENARIO:
            ScenarioGraphStore.remove(payload.scenarioID);
            // don't emit change event
            break;

        default:
        // no operation
    }
});


module.exports = {
    dispatchToken_GraphUpdate,
    ScenarioGraphStore,
};
