import { EventEmitter } from 'events';
import assign from 'object-assign';
import CHANGE_EVENT from '../constants/onEventConstants';
import { ResultsConstants } from '../constants/ResultsConstants';
import AppDispatcher from '../dispatcher/AppDispatcher';
import { MessageTypes } from '../constants/MessageConstants';
import { MessageEvents } from '../constants/MessageConstants';
import { isUndefined } from '../common/utils';
import { countItems } from '../common/utils';
import ScenarioGraphConstants from '../constants/ScenarioGraphConstants.js';
import { dispatchToken_GraphUpdate } from './ScenarioGraphStore';
import { dispatchToken_ClearResults } from './ResultsStore';


const MessageStore = assign({}, EventEmitter.prototype, {

    /**
     * @example
     * messages: [
     *    {type:MessageTypes.ERROR, message:'msg1', label:{name: 'label1'}, index:1},
     *    {type:MessageTypes.WARNING, message:'msg2', label:{name: 'label2', id:2}, index: 2}
     *  ],
     */
    messages: [],

    /**
     * Get all items in the messages array
     * @returns {array} Return all items in the messages array
     */
    getMessages: function getMessages() {
        return this.messages;
    },

    /**
     * Get the number of messages
     * @returns {Number} Returns the number of messages
     */
    getNumMessages: function getNumMessages() {
        return countItems(this.messages);
    },

    /**
     * Get all items in the messages array that satisfy a given label name.
     * The returned items will match the label.name member of a message object.
     * @param {string} labelName - The label name
     * @returns {Array} Returns an array of items in the messages array
     */
    getMessagesForLabel: function getMessagesForLabel(labelName) {
        var filteredMessages = [];
        this.messages.forEach((message) => {
            if (message.label.name === labelName) {
                filteredMessages.push(message);
            }
        });

        return filteredMessages;
    },

    /**
     * Special method for processing the error messages returned after calculating results
     * This is used for both errors from the pricer and scenario APIs.
     * @param {Array} results - Array of error messages
     * @param {string} [prefix=''] - A prefix to add to the error messages
     * @param {object} messageLabel The label assigned to the message
     */
    processResultError: function processResultError(results, prefix, messageLabel) {
        // check if a prefix has been given
        var msgPrefix = '';
        if (!isUndefined(prefix)) {
            msgPrefix = prefix;
        }
        // display the error messages returned from the pricing API
        if (!isUndefined(results.ERROR)) {
            const messages = results.ERROR;
            if (messages.length !== 0) {
                messages.forEach((m) => {
                    this.addMessage(msgPrefix + m, MessageTypes.ERROR, messageLabel);
                });
            }
        }
    },

    /**
     * Add an item to the messages array
     * @param {string} text The text to add
     * @param {MessageTypes} messageType The type of the error message
     * @param {object} messageLabel The label assigned to the message
     */
    addMessage: function addMessage(text, messageType, messageLabel) {
        const idx = this.messages.length;
        this.messages.push({ type: messageType, message: text, label: messageLabel, index: idx });
    },

    /**
     * Delete a particular message from the messages array
     * @param {number} id The index in the messages array
     */
    removeMessageById: function removeMessageById(id) {
        delete this.messages[id];
    },

    /**
     * Delete messages from the array based on the error type
     * @param {MessageTypes} MessageType The error message type
     */
    removeMessageByType: function removeMessageByType(MessageType) {
        // if MessageType is not given, all messages will be deleted.

        if (typeof(MessageType) === 'undefined') {
            this.messages = [];
        }
        else {
            var newMessage = [];
            var MessageTypeArray = MessageType;

            // if the message type is a string, convert it to a list
            if (typeof(MessageType) === 'string') {
                MessageTypeArray = [MessageType];
            }

            // only includes messages that are not of type 'MessageType'
            this.messages.forEach((m) => {
                if (MessageTypeArray.indexOf(m.type) < 0) {
                    newMessage.push(m);
                }
            });

            // update the messages
            this.messages = newMessage;
        }
    },

    emitChange: function emitChange() {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    },

});


// Separate dispatch callbacks for tokens
//const dispatchToken_RemoveMessage = AppDispatcher.register((payload) => {
//    // clear error messages before updating results
//    switch (payload.eventName) {
//
//        //case ResultsConstants.UPDATE_RESULTS:
//        //    MessageStore.removeMessageByType(payload.message_types);
//        //    MessageStore.processResultError(payload.newResults);
//        //    MessageStore.emitChange();
//        //    break;
//
//        //case MessageEvents.ADD_MESSAGE_CLEAR_RESULTS:
//        //    MessageStore.removeMessageByType(payload.message_types);
//        //    // don't emit changes
//        //    break;
//
//        default:
//        // no operation
//    }
//});


// Register callbacks to LegStore
AppDispatcher.register((payload) => {
    switch (payload.eventName) {
        // MessageStore callbacks
        case MessageEvents.DELETE_MESSAGE:
            MessageStore.removeMessageById(payload.message_id);
            MessageStore.emitChange();
            break;

        case MessageEvents.DELETE__MESSAGES_BY_TYPE:
            MessageStore.removeMessageByType(payload.message_types);
            // If call to listeners is not disabled
            if (!payload.suppress_emit) {
                MessageStore.emitChange();
            }
            break;

        // case MessageEvents.ADD_MESSAGE:
        //     MessageStore.addMessage(payload.message, payload.type, payload.message_label);
        //     MessageStore.emitChange();
        //     break;

        case ResultsConstants.UPDATE_RESULTS:

            // wait for results updates
            AppDispatcher.waitFor([dispatchToken_ClearResults]);

            // if requested, clean up previous messages
            if (payload.clear_previous_messages) {
                MessageStore.removeMessageByType(payload.message_types);
            }

            MessageStore.processResultError(payload.newResults, 'Pricer: ', payload.message_label);

            // If call to listeners is not disabled
            if (!payload.suppress_emit) {
                MessageStore.emitChange();
            }
            break;

        case MessageEvents.ADD_MESSAGE_CLEAR_RESULTS:
            //AppDispatcher.waitFor( [dispatchToken_RemoveMessage] );

            // wait for results update
            AppDispatcher.waitFor([dispatchToken_ClearResults]);

            // if requested, clean up previous messages
            if (payload.clear_previous_messages) {
                MessageStore.removeMessageByType(payload.message_types);
            }

            MessageStore.addMessage(payload.message, payload.type, payload.message_label);

            // If call to listeners is not disabled
            if (!payload.suppress_emit) {
                MessageStore.emitChange();
            }
            break;

        case ScenarioGraphConstants.UPDATE_GRAPH:
            // wait for graph updates
            AppDispatcher.waitFor([dispatchToken_GraphUpdate]);
            // then update the message store

            // If requested, clear the previous messages
            if (payload.clear_previous_messages) {
                if (process.env.NODE_ENV !== 'production') {
                    console.log('message clear');
                }
                MessageStore.removeMessageByType(payload.message_types);
            }

            const prefix = 'Profile ' + payload.msg_prefix + ': ';
            MessageStore.processResultError(payload.newResults, prefix, payload.message_label);

            // If call to listeners is not disabled
            if (!payload.suppress_emit) {
                MessageStore.emitChange();
            }
            break;

        case ScenarioGraphConstants.CLEAR_GRAPH_ADD_MESSAGE:
            // wait for graph updates
            AppDispatcher.waitFor([dispatchToken_GraphUpdate]);
            // then update the graph store
            // If requested, clear the previous messages
            if (payload.clear_previous_messages) {
                MessageStore.removeMessageByType(payload.message_types);
            }

            MessageStore.addMessage(payload.message, payload.type, payload.message_label);
            if (process.env.NODE_ENV !== 'production') {
                console.log('message add');
                console.log(payload.message);
            }

            // If call to listeners is not disabled
            if (!payload.suppress_emit) {
                MessageStore.emitChange();
            }
            break;

        default:
            // no operation
    }
});


export default MessageStore;

