import { EventEmitter } from 'events';
import assign from 'object-assign';
import CHANGE_EVENT from '../constants/onEventConstants';
import AppDispatcher from '../dispatcher/AppDispatcher';
import PricerConstants from '../constants/PricerConstants';


const PricerStore = assign({}, EventEmitter.prototype, {


    // default is true
    isValid: true,

    /**
     * Set the valid state of the pricer
     * @param {boolean} status true if the pricer is valid, otherwise false
     */
    setValid: function setValid(status) {
        this.isValid = status;
    },

    /**
     * Return the validation state of the pricer
     * @returns {boolean}
     */
    getValidationState: function getValidationState() {
        return this.isValid;
    },

    emitChange: function emitChange() {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function addChangeListener(callback) {
        if (process.env.NODE_ENV !== 'production') {
            console.log('%c PricerStore Change Listener ', 'background: #666; color: #bada55');
        }
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    },

});

//Register callbacks to GreeksCheckBoxStore
AppDispatcher.register((payload) => {
    switch (payload.eventName) {
        // PricerStore callbacks
        case PricerConstants.SET_VALID:
            PricerStore.setValid(payload.status);
            PricerStore.emitChange();
            break;

        default:
        // no operation
    }
});


export default PricerStore;