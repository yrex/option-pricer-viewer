import { EventEmitter } from 'events';
import assign from 'object-assign';
import CHANGE_EVENT from '../constants/onEventConstants';
import { ScenarioConfigConstants } from '../constants/ScenarioConfigConstants';
import { Scenario_xVals, Scenario_yVals } from '../constants/ScenarioConfigConstants';
import AppDispatcher from '../dispatcher/AppDispatcher';
import PricerConstants from '../constants/PricerConstants';
import { dispatchToken_GraphUpdate } from './ScenarioGraphStore';


const defaultConfig =  {
    x_type: Scenario_xVals[0].value,
    y_type: Scenario_yVals[0].value,
    x_min: 0,
    x_max: 100,
    num_points: 30,
    time_offset: 0,
    inc_orig_x_vals:true,
    add_support_points: true,
    scenario_valid: true,
    enabled: true,
};

const ScenarioConfigStore = assign({}, EventEmitter.prototype, {

    // configs: [],
    configs: [assign({}, defaultConfig)],
    numConfigs: 1,
    // keep track of the pricer's validation state. Set initially to true.
    pricerIsValid: true,

    /**
     * Add a new configuration to the scenario configuration array
     * @param {object} configParams The parameters for the scenario configuration
     */
    addScenario: function addScenario(configParams) {
        // configParams is an optional parameter
        if (typeof configParams === 'undefined') {
            // take a deep copy
            configParams = assign({}, defaultConfig);
        }

        // Set/override the enabled parameter according to the pricer's validation state
        configParams.enabled = this.pricerIsValid;

        this.configs.push(configParams);
        this.numConfigs += 1;
    },

    /**
     * Delete a given item from the scenario configuration array
     * @param {number} ScenarioConfigID The index of the scenario configuration to delete
     */
    removeScenario: function removeScenario(ScenarioConfigID) {
        delete this.configs[ScenarioConfigID];
        this.numConfigs -= 1;
    },

    /**
     *
     * @param {number} id The index of the item in the configs array
     * @param {string} field The field to update
     * @param value The value of the field. Data type depends on the field.
     */
    updateScenario: function updateScenario(id, field, value) {
        this.configs[id][field] = value;
    },

    /**
     * @param {boolean} status Whether to enabled or disable the scenarios
     * This method is used to enable or disable all scenarios based on the validation status of the pricer
     */
    setAllScenariosPricerStatus: function setAllScenariosPricerStatus(status) {
        // update store's record of the pricer's validation state
        this.pricerIsValid = status;

        // update the enabled status of each scenario
        this.configs.forEach((undefined, idx) => {
            this.updateScenariosEnabledStatus(idx);
        });
    },

    /**
     * @param {number} id The scenario ID
     * @param {boolean} status Whether to enabled or disable a scenario
     * This method is used to update the vlaidation status of a scenario.
     */
    setScenariosValidStatus: function setScenariosValidStatus(id, status) {
        // update the scenario's validation flag
        this.updateScenario(id, 'scenario_valid', status);
        // update the enabled status
        this.updateScenariosEnabledStatus(id);
    },

    /**
     * @param {number} id The scenario ID
     * This method is used to enable or disable a scenario.
     * A scenario is enabled if it is valid and the pricer is also valid.
     */
    updateScenariosEnabledStatus: function updateScenariosEnabledStatus(id) {
        // update the enabled field
        const isEnabled = this.getScenario(id).scenario_valid && this.pricerIsValid;
        this.updateScenario(id, 'enabled', isEnabled);
    },

    /**
     * Return the number of items in the array
     * @returns {number} The number of configs
     */
    getNumScenarios: function getNumScenarios() {
        return this.numConfigs;
    },

    /**
     * Return the a particular config object
     * @returns {object} Return the config object
     */
    getScenario: function getScenario(id) {
        return this.configs[id];
    },

    /**
     * Return the configs array
     * @returns {Array} Return the configs array
     */
    getAllScenarios: function getAllScenarios() {
        return this.configs;
    },

    emitChange: function emitChange() {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    },

});


//Register callbacks to ScenarioConfigStore
AppDispatcher.register((payload) => {
    switch (payload.eventName) {

        // ScenarioConfigStore callbacks
        case ScenarioConfigConstants.ADD_SCENARIO:
            if (process.env.NODE_ENV !== 'production') {
                console.log('here_scenario');
            }
            // wait for graph updates
            AppDispatcher.waitFor([dispatchToken_GraphUpdate]);
            // then add the scenario config
            ScenarioConfigStore.addScenario();
            ScenarioConfigStore.emitChange();
            break;

        case ScenarioConfigConstants.UPDATE_SCENARIO:
            ScenarioConfigStore.updateScenario(payload.scenarioID, payload.fieldName, payload.newValue);
            ScenarioConfigStore.emitChange();
            break;

        case ScenarioConfigConstants.REMOVE_SCENARIO:
            // wait for graph updates
            AppDispatcher.waitFor([dispatchToken_GraphUpdate]);
            // then remove the scenario config
            ScenarioConfigStore.removeScenario(payload.scenarioID);
            ScenarioConfigStore.emitChange();
            break;

        case ScenarioConfigConstants.SET_SCENARIO_VALIDATION:
            ScenarioConfigStore.setScenariosValidStatus(payload.scenarioID, payload.status);
            ScenarioConfigStore.emitChange();
            break;

        case PricerConstants.SET_VALID:
            ScenarioConfigStore.setAllScenariosPricerStatus(payload.status);
            if (process.env.NODE_ENV !== 'production') {
                console.log('done!');
            }
            ScenarioConfigStore.emitChange();
            break;

        default:
            // no operation
    }
});

export default ScenarioConfigStore;
