import { EventEmitter } from 'events';
import assign from 'object-assign';
import { CHANGE_EVENT, INIT_EVENT } from '../constants/onEventConstants';
import { LegConstants, DEFAULT_LEG_TYPE } from '../constants/LegConstants';
import AppDispatcher from '../dispatcher/AppDispatcher';


const defaultLeg = {
    callput: 'call',
    buysell: 'buy',
    price: 0,
    strike: 0,
    vol: 0,
    rate: 0,
    time: 0,
    dividend: 0,
};

const LegStore = assign({}, EventEmitter.prototype, {

    strategies: [],
    legs: [assign({}, defaultLeg)],
    numLegs: 1,
    dealName: '',

    /**
     * Add the various leg combinations (strategies).
     * @param {object} legTypes - the various strategies
     */
    initialise: function initialise(legTypes) {
        this.strategies = legTypes;
        if (process.env.NODE_ENV !== 'production') {
            console.log('LegStore types initialised');
        }
    },

    /**
     * Add a new leg to the legs array
     * @param {object} legParams The parameters for the leg
     */
    addLeg: function addLeg(legParams) {
        // legParams is an optional parameter
        if (typeof legParams === 'undefined') {
            // take a deep copy
            legParams = assign({}, defaultLeg);
        }

        this.legs.push(legParams);
        this.numLegs += 1;
    },

    /**
     * Delete a given item from the leg array
     * @param {number} legID The index of the leg to delete
     */
    removeLeg: function removeLeg(legID) {
        delete this.legs[legID];
        this.numLegs -= 1;
    },

    /**
     *
     * @param {number} id The index of the item in the leg array
     * @param {string} field The field to update
     * @param value The value of the field. Data type depends on the field.
     */
    updateLeg: function updateLeg(id, field, value) {
        this.legs[id][field] = value;
    },

    /**
     * Return the number of items in the array
     * @returns {number} The number of legs
     */
    getNumLegs: function getNumLegs() {
        return this.numLegs;
    },

    getStrategyList: function getStrategyList() {
        return this.strategies;
    },

    /**
     * Set the default leg on application startup. A default leg type (e.g. long call),
     * or a blank string will be passed.
     * The default leg type is the name given in the strategies.json file.
     */
    setDefaultLegs: function setDefaultLegs() {
        const legType = DEFAULT_LEG_TYPE || '';
        this.updateLegFromPreset(legType);
    },

    /**
     * Update the legs to match a given preset strategy. If no preset is given (i.e.
     * legType is an empty string, then use a leg with zero values.
     * The preset strategies are read from this.strategies.
     *
     * If the legType doesn't exist, the default zero-valued leg will be used.
     *
     * @param {string} legType - The leg type, corresponding to the loaded strategies,
     * or an empty string to load a leg with zero values.
     */
    updateLegFromPreset: function updateLegFromPreset(legType) {
        // Check if the leg type exists and is not an empty string
        if (legType && this.strategies[legType]) {
            // clone
            this.legs = JSON.parse(JSON.stringify(this.strategies[legType].strategy));
            this.numLegs = this.legs.length;
            this.dealName = this.strategies[legType].name;
        }
        else {
            this.legs = [assign({}, defaultLeg)];
            this.numLegs = 1;
        }
    },

    /**
     * Return the name of the current strategy.
     * @returns {string} - The strategy name
     */
    getCurrentStrategyName: function getCurrentStrategyName() {
        return this.dealName;
    },

    /**
     * Return the legs array
     * @returns {Array} Return the legs array
     */
    getAllLegs: function getAllLegs() {
        return this.legs;
    },

    emitChange: function emitChange() {
        this.emit(CHANGE_EVENT);
    },

    emitInit: function emitInit() {
        this.emit(INIT_EVENT);
    },

    addChangeListener: function addChangeListener(callback) {
        if (process.env.NODE_ENV !== 'production') {
            console.log('%c LegStore Change Listener ', 'background: #666; color: #bada55');
        }
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    },

    addInitListener: function addInitListener(callback) {
        if (process.env.NODE_ENV !== 'production') {
            console.log('%c LegStore Init Listener ', 'background: #666; color: #bada55');
        }
        this.on(INIT_EVENT, callback);
    },

    removeInitListener: function removeInitListener(callback) {
        this.removeListener(INIT_EVENT, callback);
    },

});


//Register callbacks to LegStore
AppDispatcher.register((payload) => {
    switch (payload.eventName) {

        // LegStore callbacks
        case LegConstants.ADD_LEG:
            LegStore.addLeg();
            LegStore.emitChange();
            break;

        case LegConstants.UPDATE_LEG:
            LegStore.updateLeg(payload.legID, payload.fieldName, payload.newValue);
            LegStore.emitChange();
            break;

        case LegConstants.REMOVE_LEG:
            LegStore.removeLeg(payload.legID);
            LegStore.emitChange();
            break;

        case LegConstants.INIT:
            LegStore.initialise(payload.legTypes);
            LegStore.setDefaultLegs();
            LegStore.emitInit();
            break;

        case LegConstants.SET_LEG_PRESET:
            LegStore.updateLegFromPreset(payload.presetName);
            LegStore.emitChange();
            break;

        default:
            // no operation
    }
});

export default LegStore;
