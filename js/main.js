import React from 'react';
import ReactDOM from 'react-dom';
import OptionApp from './components/OptionApp.react';

/*
 * Entry Point
 */

ReactDOM.render(
    <OptionApp />,
        document.getElementById('optpricer')
);
