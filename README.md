This project is the **Option Pricer and Risk Profiler** web-based application, hosted on [OptionProfiler.com](https://optionprofiler.com).
This is an educational tool for studying and understanding various financial option strategies.

The calculations for the option pricing and greeks is done using a Flask-based back-end.
The code for that project is available [here](https://gitlab.com/yrex/flask-option-pricer). 

![payoff profile](static/images/snapshot.png)


### Getting started
To work on this project, you need Node/NPM installed, as well as access to a good IDE.

To get started, first clone this project to a project directory on your local machine. Then in that directory, run

```
npm update
```

to install all dependencies. The application is built using the React library and other packages.
These will all be installed.

Use the scripts provided in `package.json` to build the application. To build the full production-ready application, run
```
npm run build:full
```
which will build the HTML, JavaScript and CSS files, and will also copy the static files (e.g. fonts).
The final results will be available in the `build` directory.
